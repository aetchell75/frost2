<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MyMember extends DataExtension {

    private static $db = array(
        'AccountType' => "enum('FREE,PAID,TROLL','FREE')",
    );
    private static $has_one = array(
        'Club' => 'MemberClub'
    );
    static $defaults = array(
        'AccountType' => 'FREE'
    );
    
    private static $summary_fields = array(
        'AccountType' => 'AccountType'
    );    

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab('Root.Main', new DropdownField(
                'AccountType', 'Account Type', $this->owner->dbObject('AccountType')->enumValues()
                )
        );
    }

}
