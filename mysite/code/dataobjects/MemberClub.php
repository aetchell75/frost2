<?php

class MemberClub extends DataObject {

    private static $singular_name = 'Club page for Members';
    private static $has_one = array(
        //	'Logo' => 'Image',
        'Member' => 'Member',
    );
    private static $db = array(
        'Name' => 'varchar(60)',
        'ContactName' => 'varchar(255)',
        'ContactEmail' => 'varchar(255)',
        'ContactNumber' => 'varchar(255)',
        'About' => 'Text',
        'Fees' => 'Text',
        'Rules' => 'Text',
        'Meetings' => 'Text',
        'Address' => 'Text',
        'Website' => 'Text',
        'Facebook' => 'Text',
        'Twitter' => 'Text',
        'Google' => 'Text',
        'Lat' => 'varchar(60)',
        'Lng' => 'varchar(60)',
        'public' => 'Boolean(1)'
    );

    /*
      ##Address geolocation

      You can optionally add a text field that allows the user to search for a location. A button is provided for the user to click and update the map.

      ##Options

      If your situation isn't the same as above, you can provide any of the following options to suit your setup.

      These options can be passed as key => value pairs as the second argument of the GoogleMapField function. These options and their default values are:

      $googleMapField = GoogleMapField::create("Map", array(
      "height" => "500px",                          // The height of the map element
      "heading" => "",                              // A heading in a <h4> tag to appear before the map
      "lng_field" => "Form_ItemEditForm_Lng",       // The ID of the longitude input element
      "lat_field" => "Form_ItemEditForm_Lat",       // The ID of the latitude input element
      "tab" => "Root_Location",                     // The ID of the tab that the map is in
      "address_field" => "Address",                 // The ID of the address field (if it exists)
      "map_zoom" => 10,                             // The initial zoom level of the map
      "start_lat" => "51.508515",                   // The initial latitude of the map marker
      "start_lng" => "-0.125487"                    // The initial longitude of the map marker
      ));
     */

    /*
      'Name' => 'varchar(60)',
      'ContactName' => 'varchar(255)',
      'ContactEmail' => 'varchar(255)',
      'ContactNumber' => 'varchar(255)',
      'About' => 'Text',
      'Fees' => 'Text',
      'Rules' => 'Text',
      'Meetings' => 'Text',
      'Address' => 'Text',
      'Website' => 'Text',
      'Facebook' => 'Text',
      'Twitter' => 'Text',
      'Google' => 'Text',
      'Lat' => 'varchar(60)',
      'Lng' => 'varchar(60)',
      'public' => 'Boolean'
     */

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $Name = new TextField('Name', 'Name');
        $ContactName = new TextField('ContactName', 'Contact Name');
        $ContactEmail = new TextField('ContactEmail', 'Contact Email');
        $ContactNumber = new TextField('ContactNumber', 'Contact Number');
        $About = new TextField('About', 'About');
        $Fees = new TextField('Fees', 'Club Fees');
        $Rules = new TextField('Rules', 'Club Rules');
        $Address = new TextField('Address', 'Address');
        $Meetings = new TextField('Meetings', 'Meetings');
        $Facebook = new TextField('Facebook', 'Facebook');
        $Twitter = new TextField('Twitter', 'Twitter');
        $Logo = new UploadField('Logo', 'Logo');
        $Logo->setAllowedFileCategories('image');
        $Logo->setFolderName('clublogos');

        $fields->addFieldToTab('Root.Main', $Name);
        $fields->addFieldToTab('Root.Main', $About);
        $fields->addFieldToTab('Root.Main', $Address);
        $fields->addFieldToTab('Root.Main', $Meetings);
        $fields->addFieldToTab('Root.Main', $Facebook);
        $fields->addFieldToTab('Root.Main', $Twitter);
        $fields->addFieldToTab('Root.Main', $Logo);
        return $fields;
    }

}
