<?php

class OnlineListBuilders extends DataObject {

    private static $singular_name = 'Online List Builders';
    private static $db = array(
        'Name' => 'varchar(255)',
        'URL' => 'varchar(255)',
        'Desc' => 'Text'
    );
    private static $has_one = array(
        'Image' => 'File',
    );

}
