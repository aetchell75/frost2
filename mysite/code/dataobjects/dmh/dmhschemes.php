<?php

class dmhschemes extends DataObject {

    private static $singular_name = 'DMH Scheme';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'Text',
        'Source' => "ENUM('Core,Legend,Curse,Down Under,Common')"
    );
    private static $has_one = array(
        'Faction' => 'dmhfaction'
    );
    
    private static $summary_fields = array(
        'Name' => 'Faction',
        'Faction.Name' => 'Faction',
    );       

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        return $fields;
    }
}
