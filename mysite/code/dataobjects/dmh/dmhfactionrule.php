<?php

class dmhfactionrule extends DataObject {

    private static $singular_name = 'DMH faction rule';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc'  => 'Text'
    );
    private static $has_one = array(
        'Faction' => 'dmhfaction'
    );
    
    private static $summary_fields = array(
        'Name' => 'Model',
        'Desc' => 'Desc',
        'Faction.Source' => 'Source',
    );        

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        return $fields;
    }
}
