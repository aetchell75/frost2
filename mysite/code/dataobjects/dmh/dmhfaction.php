<?php

class dmhfaction extends DataObject {

    private static $singular_name = 'DMH faction';
    private static $db = array(
        'Name' => 'varchar(255)',
        'URL'   => 'Text',
        'Primary' => 'Boolean',
        'Suit'  => "ENUM('Clubs,Spades,Diamonds,Hearts,Creature,Skulls')",
        'Source' => "ENUM('Core,Legend,Curse,Down Under')",
        'Align' => "ENUM('Good,Evil')",
    );
    private static $has_many = array(
        'Rules' => 'dmhfactionrule'
    );
    static $defaults = array(
        'Source' => 'Core',
        'Align' => 'Good',
    );
    
    private static $summary_fields = array(
        'Name' => 'Faction',
        'PrimaryNice' => 'Primary Faction',
        'Suit' => 'Suit',
        'Name' => 'Item',
        'Source' => 'Source'
    );       

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $Name = new TextField('Name', 'Faction name');
        $fields->addFieldToTab('Root.Main', $Name);
        return $fields;
    }
    
    public function PrimaryNice() {
        return $this->Primary == 1 ? 'yes' : 'no';
    }
}
