<?php

class fguserwarband extends DataObject {

    private static $singular_name = 'Warband';
    private static $db = array(
        'hash' => 'varchar(60)',
        'Funds' => 'Int',
        'WizardName' => 'varchar(255)',
        'EXP' => 'Int',
        'CEXP' => 'Int',
        'Lich' => 'Boolean',
        'Locked' => 'Boolean',
		'DemonicPact' => 'Boolean'
    );
    private static $has_one = array(
        'Member' => 'Member',
        'School' => 'fgschool',
        'Homebase' => 'fgbasetype',
        'BeastcrafterTrait' => 'fgbeastcraftertraits',
        'BeastcrafterLevel' => 'fgbeastcrafterlevels',	
    );
    private static $has_many = array(
        'WarbandUnits' => 'fguserunits',
        'Spells' => 'fguserspells',
        'Items' => 'fguseritems',
        'WizardLevels' => 'fguserwizardlevels',
        'CaptainTricks' => 'fgusertricks',
        'BaseResources' => 'fguserresources',
        'Events' => 'fguserwarbandlog',
		'Notes'	=> 'fguserwarbandnotes',
		'Boons' => 'fguserboons',
		'Sacrifices' => 'fgusersacrifices'
    );
    private static $summary_fields = array(
        'UserName' => 'Owner',
        'WizardName' => 'Wizard Name',
        'getSchool' => 'School',
        'Level' => 'Wizard Level',
        'Funds' => 'Funds',
        'EXP' => 'EXP',
        'CEXP' => 'Captains Exp'
    );
    static $defaults = array(
        'Funds' => 500,
        'LevelMod' => 0,
        'Locked' => 0
    );

    //private static $default_sort = 'LastEdited Desc';

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('hash');
        $WizardName = new TextField('WizardName', 'Wizard Name');
        $Funds = new TextField('Funds', 'Funds');
        $EXP = new TextField('EXP', 'Experience Points');
        $CEXP = new TextField('CEXP', 'Captain Experience Points');

        $fields->addFieldToTab('Root.Main', $WizardName);
        $fields->addFieldToTab('Root.Main', $Funds);
        $fields->addFieldToTab('Root.Main', $EXP);
        $fields->addFieldToTab('Root.Main', $CEXP);

        return $fields;
    }

    public function Cash() {
        return number_format($this->Funds);
    }

    public function onAfterWrite() {
        parent::onAfterWrite();
        if ($this->hash == '') {
            $this->hash = md5($this->ID);
            $this->write();
        }
    }

    public function isLocked() {
        if ($this->Locked == 0) {
            return false;
        }
        return true;
    }

    public function getUserName() {
        return $this->Member()->FirstName . ' ' . $this->Member()->Surname;
    }

    public function getSchool() {
        return $this->School()->Name;
    }

    public function getLevel() {
        $applied = $this->WizardLevels()->filter('CountAsLevel', array(1))->Count();
        return $applied;
        //return floor($this->EXP / 100);
    }

    public function getCLevel() {
        $tricks = $this->CaptainTricks()->Count();
        $total = $tricks - 2;
        if ($total < 0) {
            $total = 0;
        }
        return $total;
        //return floor($this->CEXP / 100);
    }

    public function secureID() {
        return ($this->hash);
    }

    public function getSortedUnits() {
        $units = $this->WarbandUnits()->sort('fgunit.Sort');
        return $units;
    }

    public function getWarbandSoldiers() {
        $units = $this->WarbandUnits()->filter(array('Type' => array('Soldier', 'Monster')))->sort(array('UnitID.Sort'=>'ASC'));
        return $units;
    }

    public function getWizard() {
        $wizard = $this->WarbandUnits()->filter(array('Type' => 'Wizard'))->first();
        return $wizard;
    }

    public function getApprentice() {
        $apprentice = $this->WarbandUnits()->filter(array('Type' => 'Apprentice'))->first();
        return $apprentice;
    }

//    public function apprenticeCost() {
//        return (int) (($this->getLevel() - 10) * 10) + 300;
//    }

    public function getCaptain() {
        if ($this->hasCaptain() != false) {
            $captain = $this->WarbandUnits()->filter(array('Type' => 'Captain'))->first();
            return $captain;
        }
    }

    //returns the captains unitID
    public function hasCaptain() {
        $captain = $this->WarbandUnits()->filter(array('Type' => 'Captain'))->first();
        return $captain;
    }
    
    public function hasMount() {
        $mount = $this->WarbandUnits()->filter(array('Type' => 'Soldier'));
		foreach($mount as $k => $v) {
			if ($v->Unit()->Class == 'Mount') {
				return $v;	
			}
		}
        return false;
    }   

	public function getMounts() {
        $mount = $this->WarbandUnits()->filter(array('Type' => 'Soldier'));
		$mountArr = new ArrayList;
		foreach($mount as $k => $v) {
			if ($v->Unit()->Class == 'Mount') {	
				$mountArr->push(new ArrayData(array('item' =>$v)));			
			}
		}
        return $mountArr;		
	}	

    public function canMutateAnimal() {
		$canMutate = $this->Spells()->filter(array('SpellID' => 86))->first();
		if($canMutate) {
			return true;
		} 
		return false;
		/*
        foreach ($this->Spells() as $spell) {
            if ($spell->Spell()->ID == 86) {
                return true;
            }
        }
		*/
    }
    
    public function canBrand() {
		$canBrand = $this->Spells()->filter(array('SpellID' => 93))->first();
		if($canBrand) {
			return true;
		} 
		return false;
		/*		
        foreach ($this->Spells() as $spell) {
            if ($spell->Spell()->ID == 93) {
                return true;
            }
        }
		*/
    }

    public function canUpgradeConstruct() {
        foreach ($this->Items() as $item) {
            if (in_array($item->Item()->ID,array(127,147))) {
                return true;
            }
        }
    }	
	
    public function canHeal() {
		$can = $this->Spells()->filter(array('SpellID' => 40))->first();
		if($can) {
			return true;
		} 
		return false;		
        // now check spells
        // SPELL ID 40 = miraculous cure
		/*
        foreach ($this->Spells() as $spell) {
            if ($spell->Spell()->ID == 40) {
                return true;
            }
        }
		*/
    }

    public function nigglingDiscount() {
        $discount = 0;
        foreach ($this->getWarbandSoldiers() as $soldier) {
            if ($soldier->Unit()->Healer >= 1) {
                $discount = $discount + 10;
            }
        }
        return $discount;
    }

    public function hasApprentice() {
        foreach ($this->WarbandUnits() as $unit) {
            if ($unit->Type == 'Apprentice') {
                return $unit->UnitID;
            }
        }
        return false;
    }

    public function VaultItems() {
        $items = $this->Items()->filter(array('AssignedID:LessThanOrEqual' => 0));
        return $items;
    }

    public function levelOptions() {
        $options = array();
        foreach ($this->Spells() as $spell) {
            $options[] = new ArrayData(array(
                'Name' => ucfirst(strtolower($spell->Spell()->Name)),
                'SpellID' => $spell->Spell()->ID,
                'Type' => 'Spell'
            ));
        }
        foreach (array('Fight', 'Shoot', 'Will', 'Health') as $item) {
            $options[] = new ArrayData(array(
                'Name' => $item,
                $item => 1,
                'Type' => $item
            ));
        }

        // add Lich
        $options[] = new ArrayData(array(
            'Name' => 'Become a Lich',
            'Type' => 'Lich',
            'ID' => 1
        ));
        $options[] = new ArrayData(array(
            'Name' => '-1 level, -1 Health',
            'Type' => 'LichFail',
            'ID' => 5
        ));
        $options[] = new ArrayData(array(
            'Name' => '-3 levels, -2 health, -1 Will, 1 Injury',
            'Type' => 'LichFail',
            'ID' => 10
        ));
        $options[] = new ArrayData(array(
            'Name' => '-5 levels, -3 health, -2 Will, 2 Injuries',
            'Type' => 'LichFail',
            'ID' => 15
        ));
        $options[] = new ArrayData(array(
            'Name' => '-8 levels, -6 health, -2 Will, 3 Injuries',
            'Type' => 'LichFail',
            'ID' => 20
        ));

        foreach (fgbeastcrafterlevels::get()->sort('MinLevel ASC') as $beastcrafter) {
            if ($this->getLevel() >= $beastcrafter->MinLevel && $this->BeastcrafterLevelID < $beastcrafter->ID) {
                    $options[] = new ArrayData(array(
                        'Name' => $beastcrafter->Name,
                        'SpellID' => $beastcrafter->Spell()->ID,
                        'Type' => 'Beastcrafter',
                        'ID' => $beastcrafter->ID
                    ));                    

            }
        }
        //print_r($options);
        return new ArrayList($options);
    }

    public function beastcrafterTraits() {
        if ($this->BeastcrafterLevelID == 3) {
            $traits = DataObject::get('fgbeastcraftertraits')->filter('ClassName', array('fgbeastcraftertraits'));
            return $traits;
        }
        return false;
    }

    public function trickOptions() {
        $options = array();
        $myTricks = array();
        foreach ($this->CaptainTricks() as $wbtrick) {
            $myTricks[] = $wbtrick->ID;
        }
        foreach (DataObject::get('fgcaptaintricks') as $trick) {

            if (!in_array($trick->ID, $myTricks)) {
                $options[] = new ArrayData(array(
                    'Name' => $trick->Name,
                    'TrickID' => $trick->ID,
                    'Type' => 'Trick'
                ));
            }
        }
        foreach (array('Will','Health') as $item) {
            $options[] = new ArrayData(array(
                'Name' => $item,
                $item => 1,
                'Type' => $item
            ));
        }
        //print_r($options);
        return new ArrayList($options);
    }

    public function learnableGrimoires() {
        $options = array();
        foreach ($this->Items() as $item) {
            $knownSpells = array();
            foreach ($this->Spells() as $knownSpell) {
                $knownSpells[] = $knownSpell->Spell()->ID;
            }
            if (($item->Item()->Type == 'grimoire') && (!in_array($item->Spell()->ID, $knownSpells))) {
                $options[] = new ArrayData(array(
                    'Name' => ucfirst(strtolower($item->Spell()->Name)),
                    'SpellID' => $item->SpellID,
                    'Type' => 'Grimoire',
                    'ItemID' => $item->ID
                ));
            }
        }
        //print_r($options);
        return new ArrayList($options);
    }

    public function ownedGrimoires() {
        $options = array();
        foreach ($this->Items() as $item) {
            if ($item->Item()->Type == 'grimoire') {
                $options[] = new ArrayData(array(
                    'Name' => ucfirst(strtolower($item->Spell()->Name)),
                    'SpellID' => $item->SpellID,
                    'Type' => 'Grimoire'
                ));
            }
        }
        //print_r($options);
        return new ArrayList($options);
    }

    public function logEvent($type = false, $id, $note) {
        $event = new fguserwarbandlog();
        $event->WarbandID = $this->ID;
        $object = $type . 'ID';
        $event->{$object} = $id;
        $event->Note = $note;
        $event->write();
    }

    public function levelLog() {
        return $this->Events()->filter('LevelID:GreaterThan', 0);
    }
	
	public function canForgePact() {
		$pact = $this->Items()->filter(array('ItemID' => 134))->first();
		if($pact) {
			return true;
		}
		return false;
	}

}
