<?php

class fgcaptaintricks extends DataObject {

    private static $singular_name = 'Frostgrave Tricks of the Trade (Captains)';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'Text',
        'Declare' => 'Text',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    
    private static $summary_fields = array(
        'Name' => 'Name',
        'Desc' => 'Desc',
        'Declare' => 'When to Declare'
    );
    
    static $defaults = array(  
        'Source' => 'Sellsword'
    );    

}
