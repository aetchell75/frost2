<?php

class fgusersacrifices extends DataObject {

    private static $singular_name = 'Warband Demonic Sacrifices';

    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Sacrifice' => 'fgsacrifice'
    );

}
