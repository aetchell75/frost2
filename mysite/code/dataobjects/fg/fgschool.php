<?php

class fgschool extends DataObject {

    private static $singular_name = 'Frostgrave School of Magic';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Type' => "ENUM('Primary, Secondary','Primary')",
        'Aligned_1' => 'Int',
        'Aligned_2' => 'Int',
        'Aligned_3' => 'Int',
        'Aligned_4' => 'Int',
        'Neutral_1' => 'Int',
        'Neutral_2' => 'Int',
        'Neutral_3' => 'Int',
        'Neutral_4' => 'Int',
        'Neutral_5' => 'Int',
        'Neutral_6' => 'Int',
        'Neutral_7' => 'Int',
        'Neutral_8' => 'Int',
        'Neutral_9' => 'Int',
        'Opposed_1' => 'Int',
        'Base_1'    => 'Int',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    private static $has_many = array(
        'Spells' => 'fgspell'
    );
    static $defaults = array(
        'Source' => 'Core',
        'Type' => 'Primary'
    );
    
    private static $summary_fields = array(
        'Name' => 'Item',
        'Spells.Count' => 'Num spells',
        'Source' => 'Source'
    );       

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $Name = new TextField('Name', 'School Name');
        if ($Spells = DataObject::get('fgschool')) {
            $SpellList = $Spells->map('ID', 'Name', 'Select');
        } else {
            $Spells = array(0 => 'No Schools yet');
        }

        $a1 = DropdownField::create('Aligned_1', 'Aligned', $SpellList)->setEmptyString('Choose school');
        $a2 = DropdownField::create('Aligned_2', 'Aligned', $SpellList)->setEmptyString('Choose school');
        $a3 = DropdownField::create('Aligned_3', 'Aligned', $SpellList)->setEmptyString('Choose school');
        $a4 = DropdownField::create('Aligned_4', 'Aligned', $SpellList)->setEmptyString('Choose school');

        $n1 = DropdownField::create('Neutral_1', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n2 = DropdownField::create('Neutral_2', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n3 = DropdownField::create('Neutral_3', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n4 = DropdownField::create('Neutral_4', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n5 = DropdownField::create('Neutral_5', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n6 = DropdownField::create('Neutral_6', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n7 = DropdownField::create('Neutral_7', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n8 = DropdownField::create('Neutral_8', 'Neutral', $SpellList)->setEmptyString('Choose school');
        $n9 = DropdownField::create('Neutral_9', 'Neutral', $SpellList)->setEmptyString('Choose school');

        $o1 = DropdownField::create('Opposed_1', 'Opposed', $SpellList)->setEmptyString('Choose school');
        
        $b1 = DropdownField::create('Base_1', 'Base', $SpellList)->setEmptyString('Choose school');

        $fields->addFieldToTab('Root.Main', $Name);

        $fields->addFieldToTab('Root.Main', $a1);
        $fields->addFieldToTab('Root.Main', $a2);
        $fields->addFieldToTab('Root.Main', $a3);
        $fields->addFieldToTab('Root.Main', $a4);

        $fields->addFieldToTab('Root.Main', $n1);
        $fields->addFieldToTab('Root.Main', $n2);
        $fields->addFieldToTab('Root.Main', $n3);
        $fields->addFieldToTab('Root.Main', $n4);
        $fields->addFieldToTab('Root.Main', $n5);
        $fields->addFieldToTab('Root.Main', $n6);
        $fields->addFieldToTab('Root.Main', $n7);
        $fields->addFieldToTab('Root.Main', $n8);
        $fields->addFieldToTab('Root.Main', $n9);

        $fields->addFieldToTab('Root.Main', $o1);
        
        $fields->addFieldToTab('Root.Main', $b1);

        return $fields;
    }

}
