<?php
class fgtraits extends fgbeastcraftertraits {
    private static $singular_name = 'Frostgrave Traits';
    private static $db = array(
        'Class' => "ENUM('Animal,Construct,Demon,Sapien,Mount,Knight','Sapien')",
        'Note' => 'Text'
    );
    
    static $defaults = array(
        'Class' => 'Sapien'
    );
    private static $summary_fields = array(
        'Name' => 'Name',
        'Class' => 'Class',
        'Move' => 'Move',
        'Fight' => 'Fight',
        'Shoot' => 'Shoot',
        'Armour' => 'Armour',
        'Will' => 'Will',
        'Health' => 'Health',
        'DamageF' => 'Damage (F)',
        'DamageS' => 'Damage (S)',
        'Note' => 'Note',        
        
    );    
}