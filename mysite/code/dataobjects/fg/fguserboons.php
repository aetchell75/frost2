<?php

class fguserboons extends DataObject {

    private static $singular_name = 'Warband Demonic Boons';

    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Boon' => 'fgboon'
    );

}
