<?php

class fgbeastcrafterlevels extends DataObject {

    private static $singular_name = 'Frostgrave Beastcrafter Levels';
    private static $has_one = array(
        'Spell' => 'fgspell'
    );
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'HTMLText',
        'MinLevel' => 'Int',
        'Cast'      => 'Int',
        'SoldierCost' => 'Int',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    static $defaults = array(
        'MinLevel' => '5',       
        'Source' => 'Breeding Pits',
        'Cast' => 0,
        'SoldierCost' => 0
    );
    private static $summary_fields = array(
        'Name' => 'Item',
        'Desc' => 'Desc',
        'MinLevel' => 'MinLevel',
        'Source' => 'Source',
        'SoldierCost' => 'SoldierCost',
    );

}
