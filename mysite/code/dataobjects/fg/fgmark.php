<?php

class fgmark extends DataObject {

    private static $singular_name = 'Frostgrave Marks';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'Text',
        'Type' => "ENUM('Burning,Devotional')",
        'Move' => 'Decimal',
        'Fight' => 'Int',
        'Shoot' => 'Int',
        'Armour' => 'Int',
        'Will' => 'Int',
        'Health' => 'Int',
        'DamageF' => 'Int',
        'DamageS' => 'Int',        
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster','Forgotten Pacts')"
    );
    
    private static $summary_fields = array(
        'Name' => 'Name',
        'Type' => 'Type',
        'Desc' => 'Desc',
        'Move' => 'Move',
        'Fight' => 'Fight',
        'Shoot' => 'Shoot',
        'Armour' => 'Armour',
        'Will' => 'Will',
        'Health' => 'Health',        
        'Source' => 'Source'
    );    
    
    
    static $defaults = array(
        'Type' => 'Devotional',
        'Move' => 0,
        'Fight' => 0,
        'Shoot' => 0,
        'Armour' => 0,
        'Will' => 0,
        'Health' => 0,
        'DamageF' => 0,
        'DamageS' => 0,
        'Source' => 'Forgotten Pacts'
    );    

}
