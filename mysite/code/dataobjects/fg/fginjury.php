<?php

class fginjury extends DataObject {

    private static $singular_name = 'Frostgrave Injury';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'Text',
        'Type' => "enum('Perm, Temp, Niggling','Perm')",
        'Max' => 'Int',
        'Move' => 'Decimal',
        'Fight' => 'Int',
        'Shoot' => 'Int',
        'Will' => 'Int',
        'Health' => 'Int',
        'Special' => 'Int',
        'Cast' => 'Int',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    static $defaults = array(
        'Max' => 2,
        'Move' => null,
        'Fight' => null,
        'Shoot' => null,
        'Will' => null,
        'Health' => null,
        'Special' => null,
        'Cast' => null,
        'Source' => 'Core'
    );
    private static $summary_fields = array(
        'Name' => 'Item',
        'Desc' => 'Desc',
        //'Desc' => 'Desc',
        'Type' => 'Type',
        'Max' => 'Max',
        'Move' => 'Move',
        'Fight' => 'Fight',
        'Shoot' => 'Shoot',
        'Armour' => 'Armour',
        'Will' => 'Will',
        'Health' => 'Health',
        'Special' => 'Special',
        'Source' => 'Source'
    );    

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', new TextField('Name', 'Injury Name'));
        $InjuryTypes = $this->dbObject('Type')->enumValues();
        $fields->addFieldToTab('Root.Main', new DropdownField(
                'Type', 'Injury Type', $InjuryTypes));
        $fields->addFieldToTab('Root.Main', new NumericField('Max', 'Max no. of this type'));
        $fields->addFieldToTab('Root.Main', new NumericField('Move', 'Move penalty'));
        $fields->addFieldToTab('Root.Main', new NumericField('Fight', 'Fight penalty'));
        $fields->addFieldToTab('Root.Main', new NumericField('Shoot', 'Shoot penalty'));
        $fields->addFieldToTab('Root.Main', new NumericField('Will', 'Will penalty'));
        $fields->addFieldToTab('Root.Main', new NumericField('Health', 'Health penalty'));
        $fields->addFieldToTab('Root.Main', new NumericField('Special', 'Special penalty - used to fudge odd rules'));
        $fields->addFieldToTab('Root.Main', new NumericField('Cast', 'Cast penalty'));


        $fields->addFieldToTab('Root.Main', new TextareaField('Desc', 'Injury Description'));

        return $fields;
    }

}
