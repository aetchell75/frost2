<?php

class fguserwarbandnotes extends DataObject {

    private static $singular_name = 'Warband Notes';
    private static $has_one = array(
        'Warband' => 'fguserwarband',

    );   
    private static $db =array(
        'Note'  => 'Text',
		'Date'	=> 'Date',
    );
}
