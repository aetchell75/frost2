<?php

class fguserwizardlevels extends DataObject {

    private static $singular_name = 'Wizard Levels';
    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Spell'   => 'fgspell'
    );
    
    private static $db = array(
        'Move'      => 'Decimal',
        'Fight'     => 'Int',
        'Shoot'     => 'Int',
        'Armour'    => 'Int',
        'Will'      => 'Int',
        'Health'    => 'Int',
        'Damage'    => 'Int',
        'Cast'      => 'Int',
        'EXP'       => 'Int',
        'Level'     => 'Int',
        'NewSpell'  => 'Boolean',
        'BeastCrafter' => 'Boolean',
        'Lich' => 'Boolean',
        'CountAsLevel' => 'Boolean',
        'Source'    => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations')"    
    );    
    
    static $defaults = array(
        'NewSpell' => 0,
        'BeastCrafter' => 0,
        'Lich' => 0,
        'EXP' => -100,
        'CountAsLevel' => 1
    );
    
    public function LevelAdjust() {
        return 0;
    }
}
