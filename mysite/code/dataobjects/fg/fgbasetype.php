<?php

class fgbasetype extends DataObject {

    private static $singular_name = 'Frostgrave Base Type';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'Text',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    
    static $defaults = array(
        'Source' => 'Core'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', new TextField('Name', 'Type Name'));
        $fields->addFieldToTab('Root.Main', new TextareaField('Desc', 'Type Description'));

        return $fields;
    }

}
