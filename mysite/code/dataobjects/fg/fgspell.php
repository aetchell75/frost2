<?php

class fgspell extends DataObject {

    private static $singular_name = 'Frostgrave Spell';
    private static $has_one = array(
        'School' => 'fgschool',
    );
//    private static $belongs_many_many = array(
//        'Warband' => 'fguserwarband'
//    );

    /**
     *
     * DB fields
     * @var array
     */
    private static $db = array(
        'Name' => 'varchar(255)',
        'Diff' => 'Int',
        'Type' => "enum('Touch, Out of Game, Line of Sight, Self Only, Area Effect, Out of Game/Touch, Reaction, Reaction OR Line of Sight','Touch')",
        'Desc' => 'Text',
        'StartingSpell' => "enum('Y, N','Y')",
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    static $defaults = array(
        'Source' => 'Core'
    );
    private static $summary_fields = array(
        'Name' => 'Spell',
        'School.Name' => 'School',
        'Diff' => 'Difficulty',
        'Type' => 'Type',
        'Source' => 'Source'
    );

    public function getSchoolName() {
        return $this->School()->Name;
    }

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', new TextField('Name', 'Spell Name'));
        $fields->addFieldToTab('Root.Main', new NumericField('Diff', 'Spell Difficulty'));
        $SpellTypes = $this->dbObject('Type')->enumValues();
        $fields->addFieldToTab('Root.Main', new DropdownField(
                'Type', 'Spell Type', $SpellTypes));
        $fields->addFieldToTab('Root.Main', new TextareaField('Desc', 'Spell Description'));
        $StartingSpell = $this->dbObject('StartingSpell')->enumValues();
        $fields->addFieldToTab('Root.Main', new DropdownField(
                'StartingSpell', 'Available at Start?', $StartingSpell));

        return $fields;
    }

}
