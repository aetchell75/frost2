<?php

class fgbeastcraftertraits extends DataObject {

    private static $singular_name = 'Frostgrave Beastcrafter traits';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'Text',
        'Move' => 'Decimal',
        'Fight' => 'Int',
        'Shoot' => 'Int',
        'Armour' => 'Int',
        'Will' => 'Int',
        'Health' => 'Int',
        'DamageF' => 'Int',
        'DamageS' => 'Int',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    static $defaults = array(
        'Move' => 0,
        'Fight' => 0,
        'Shoot' => 0,
        'Armour' => 0,
        'Will' => 0,
        'Health' => 0,
        'DamageF' => 0,
        'DamageS' => 0,
        'Source' => 'Breeding Pits'
    );
    private static $summary_fields = array(
        'Name' => 'Name',
        'Desc' => 'Desc',
        'Source' => 'Source'
    );

}
