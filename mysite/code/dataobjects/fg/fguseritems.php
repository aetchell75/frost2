<?php

class fguseritems extends DataObject {

    private static $singular_name = 'User Items';
    private static $db = array(
        'Equipped' => 'Boolean'
    );
    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Item' => 'fgitem',
        'Spell' => 'fgspell',
        'Assigned' => 'fguserunits',
    );
    static $defaults = array(
        'Equipped' => 0
    );
}
