<?php

class fguserunits extends DataObject {

    private static $singular_name = 'User Units';
    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Unit' => 'fgunit',
        'Trait' => 'fgtraits',
        'Mark' => 'fgmark'
    );
    private static $has_many = array(
        'Injuries' => 'fguserinjuries',
    );
    private static $db = array(
        'Type' => "enum('Wizard, Apprentice, Captain, Soldier,Monster','Soldier')",
        'Name' => 'Varchar(255)',
        'Injured' => 'Boolean',
        'Sort' => 'Int',
        'Hits'  => 'Int',
        'Illusion' => 'Int',
		'MountID' => 'Int',
        'UserNotes' => 'Text',
		'ImgURL' => 'Text'
    );
    private static $summary_fields = array(
        'Name' => 'Name',
        'Illusion' => 'Illusion',
        'Injured' => 'Injured',
        'Unit.Type' => 'Type',
    );
    
    
    
    private static $defaults = array(
        'Type' => 'Soldier'
    );
    private static $default_sort = 'Sort ASC';

    public function isInjured() {
        return $this->Injured == 1 ? true : false;
    }
    
    public function WizardName() {
        return (isset($this->Name) && $this->Name != '' ? $this->Name : $this->Warband()->WizardName);
    }
    
    public function __construct($record = null, $isSingleton = false, $model = null) {
        parent::__construct($record, $isSingleton, $model);
        $this->adjustedStats($this);
    }

    public function numItemsAssigned() {
        return DataObject::get('fguseritems')->filter('AssignedID', $this->ID)->Count();
    }

    public function itemsAssigned() {
        $items = DataObject::get('fguseritems')->filter('AssignedID', $this->ID);
        return $items;
    }

    public function onBeforeDelete() {
        parent::onBeforeDelete();
        $items = $this->itemsAssigned();
        foreach ($items as $item) {
            $item->AssignedID = 0;
            $item->write();
        }
        $injuries = $this->Injuries();
        foreach ($injuries as $injury) {
            $injury->delete();
        }
    }

    public function onBeforeWrite() {
        parent::onBeforeWrite();
        $this->Sort = $this->Unit()->Sort;
    }

    public function canAssign() {
        $items = DataObject::get('fguseritems')->filter('AssignedID', $this->ID)->Count();
        if ($items >= $this->Unit()->Items) {
            return false;
        }
        return true;
    }

    public function healthPips() {
        $c = 1;
        $arr = array();
        //while ($c <= $this->Unit()->Health) {
        while ($c <= $this->HealthAdjusted) {
            $arr[] = new ArrayData(array('health' => $c));
            $c++;
        }
        return new ArrayList($arr);
    }
    
    public function isIllusion() {
        return $this->Illusion == 1 ? true : false;
    }
    
    protected function adjustedStats(&$obj) {
        $obj->MoveAdjusted      = $obj->Unit()->Move;
        $obj->FightAdjusted     = $obj->Unit()->Fight;
        $obj->ShootAdjusted     = $obj->Unit()->Shoot;
        $obj->ArmourAdjusted    = $obj->Unit()->Armour;
        $obj->WillAdjusted      = $obj->Unit()->Will;
        $obj->HealthAdjusted    = $obj->Unit()->Health;
        $obj->CastAdjusted      = $obj->Unit()->Cast;
        $obj->LevelAdjusted     = 0;
        
        // add in the level stuff
        
        $this->adjustedStatsLevels($obj);
        $this->adjustedStatsItems($obj);
        $this->adjustedStatsInjuries($obj);
		if($obj->MarkID >= 1 ) {
			$this->adjustedStatsMarks($obj);
		}
		if($obj->MountID >= 1 ){
			$this->AdjustedStatsMount($obj);	
		}
		
		if($obj->Type == 'Wizard') {
			$this->adjustedStatsBoons($obj);
			$this->adjustedStatsSacrifices($obj);
		}
		
        if($this->isIllusion() == 1) {
            $obj->HealthAdjusted = 1;
            $obj->Unit()->Health = 1;
        }
		if(in_array($this->Unit()->ID,array(38,39,41,42,43,46))) {
			$obj->WillAdjusted = $obj->WillAdjusted + 2;
		}
    }
	
	protected function adjustedStatsBoons(&$obj) {
		//print $obj->Warband()->DemonicPact;
		if($obj->Warband()->DemonicPact == 1) {
			$boons = $obj->Warband()->Boons();
			foreach($boons as $boon) {
				$obj->MoveAdjusted      = $obj->MoveAdjusted + $boon->Boon()->Move;
				$obj->FightAdjusted     = $obj->FightAdjusted + $boon->Boon()->Fight;
				$obj->ShootAdjusted     = $obj->ShootAdjusted + $boon->Boon()->Shoot;
				$obj->ArmourAdjusted    = $obj->ArmourAdjusted + $boon->Boon()->Armour;
				$obj->WillAdjusted      = $obj->WillAdjusted + $boon->Boon()->Will;
				$obj->HealthAdjusted    = $obj->HealthAdjusted + $boon->Boon()->Health;
				$obj->CastAdjusted      = $obj->CastAdjusted + $boon->Boon()->Cast; 			
			}			
		}
	}
	
	protected function adjustedStatsSacrifices(&$obj) {
		if($obj->Warband()->DemonicPact == 1) {
			$boons = $obj->Warband()->Sacrifices();
			foreach($boons as $boon) {
				$obj->MoveAdjusted      = $obj->MoveAdjusted + $boon->Sacrifice()->Move;
				$obj->FightAdjusted     = $obj->FightAdjusted + $boon->Sacrifice()->Fight;
				$obj->ShootAdjusted     = $obj->ShootAdjusted + $boon->Sacrifice()->Shoot;
				$obj->ArmourAdjusted    = $obj->ArmourAdjusted + $boon->Sacrifice()->Armour;
				$obj->WillAdjusted      = $obj->WillAdjusted + $boon->Sacrifice()->Will;
				$obj->HealthAdjusted    = $obj->HealthAdjusted + $boon->Sacrifice()->Health;
				$obj->CastAdjusted      = $obj->CastAdjusted + $boon->Sacrifice()->Cast; 			
			}			
		}
	}	
	
	/*
	 * Is this unit mounted, if so, get stats for the mount and add to the unit
	 */
	protected function adjustedStatsMount(&$obj) {
		if($obj->MountID >= 1 ) { // unit appears to be mounted
			$mount = $obj->Warband()->WarbandUnits()->filter(array('ID' => $obj->MountID))->first();
			
			// add the basic stats for mounted
            $obj->MoveAdjusted      = $obj->MoveAdjusted + 2;
            $obj->FightAdjusted     = $obj->FightAdjusted + 1;
            $obj->ShootAdjusted     = $obj->ShootAdjusted + -2;
            $obj->ArmourAdjusted    = $obj->ArmourAdjusted;
            $obj->WillAdjusted      = $obj->WillAdjusted;
            $obj->HealthAdjusted    = $obj->HealthAdjusted;
            $obj->CastAdjusted      = $obj->CastAdjusted; 
			foreach($mount->ItemsAssigned() as $item){
				if($item->Equipped == 1) {
					$obj->MoveAdjusted      = $obj->MoveAdjusted + $item->Item()->Move;
					$obj->FightAdjusted     = $obj->FightAdjusted + $item->Item()->Fight;
					$obj->ShootAdjusted     = $obj->ShootAdjusted + $item->Item()->Shoot;
					$obj->ArmourAdjusted    = $obj->ArmourAdjusted + $item->Item()->Armour;
					$obj->WillAdjusted      = $obj->WillAdjusted + $item->Item()->Will;
					$obj->HealthAdjusted    = $obj->HealthAdjusted + $item->Item()->Health;
					$obj->CastAdjusted      = $obj->CastAdjusted + $item->Item()->Cast; 					
				}
 		
			}
  			
		}
	}
    
    protected function adjustedStatsLevels(&$obj) {
        /*
         * Is this a wizard
         */
        if(in_array($obj->Type,array('Wizard','Apprentice'))){
          foreach($obj->Warband()->WizardLevels()->filter(array('SpellID' =>0)) as $item) {
            $obj->MoveAdjusted      = $obj->MoveAdjusted + $item->Move;
            $obj->FightAdjusted     = $obj->FightAdjusted + $item->Fight;
            $obj->ShootAdjusted     = $obj->ShootAdjusted + $item->Shoot;
            $obj->ArmourAdjusted    = $obj->ArmourAdjusted + $item->Armour;
            $obj->WillAdjusted      = $obj->WillAdjusted + $item->Will;
            $obj->HealthAdjusted    = $obj->HealthAdjusted + $item->Health;
            $obj->CastAdjusted      = $obj->CastAdjusted + $item->Cast;    
            $obj->LevelAdjusted     = $obj->LevelAdjusted + $item->Level;   
            
          }
          
            $obj->Move      = $obj->MoveAdjusted ;
            $obj->Fight     = $obj->FightAdjusted;
            $obj->Shoot     = $obj->ShootAdjusted;
            $obj->Armour    = $obj->ArmourAdjusted;
            $obj->Will      = $obj->WillAdjusted;
            $obj->Health    = $obj->HealthAdjusted;             
          
          $this->adjustedStatsBeastcrafter($obj); 
        /*
         * Is this a captain
         */
        } else if ($obj->Type == 'Captain'){
            foreach($obj->Warband()->CaptainTricks()->filter(array('TrickID' => 0)) as $item) {
//            $obj->MoveAdjusted      = $obj->MoveAdjusted + $item->Move;
//            $obj->FightAdjusted     = $obj->FightAdjusted + $item->Fight;
//            $obj->ShootAdjusted     = $obj->ShootAdjusted + $item->Shoot;
//            $obj->ArmourAdjusted    = $obj->ArmourAdjusted + $item->Armour;
              $obj->WillAdjusted      = $obj->WillAdjusted + $item->Will;
            
              $obj->HealthAdjusted    = $obj->HealthAdjusted + $item->Health;
//            $obj->CastAdjusted      = $obj->CastAdjusted + $item->Cast;              
            }
            $obj->Will      = $obj->WillAdjusted;
        } 
        $obj->Move      = $obj->MoveAdjusted ;
        $obj->Fight     = $obj->FightAdjusted;
        $obj->Shoot     = $obj->ShootAdjusted;
        $obj->Armour    = $obj->ArmourAdjusted;
        $obj->Will      = $obj->WillAdjusted;
        $obj->Health    = $obj->HealthAdjusted;          
        $this->adjustedStatsTraits($obj);         
        /*
         * Don't apply levels for other unit types
         */
    }       

    protected function adjustedStatsInjuries(&$obj) {
        foreach($obj->Injuries() as $item) {
            if(
                ($item->Injury()->Type == 'Niggling' && $item->Treated != 1)
                || $item->Injury()->Type == 'Perm'
            ){
                $obj->MoveAdjusted      = $obj->MoveAdjusted + ($item->Injury()->Move * $item->Num);
                $obj->FightAdjusted     = $obj->FightAdjusted + ($item->Injury()->Fight * $item->Num);
                $obj->ShootAdjusted     = $obj->ShootAdjusted + ($item->Injury()->Shoot * $item->Num);
                $obj->ArmourAdjusted    = $obj->ArmourAdjusted + ($item->Injury()->Armour * $item->Num);
                $obj->WillAdjusted      = $obj->WillAdjusted + ($item->Injury()->Will * $item->Num);
                if($item->Injury()->Special == 1 && $item->Num > 1) {
                    // exception for niggling injury
                    $obj->HealthAdjusted    = $obj->HealthAdjusted + ($item->Injury()->Health - 1);
                } else if($item->Injury()->Special == 1 && $item->Num <= 1) {
                    $obj->HealthAdjusted    = $obj->HealthAdjusted + ($item->Injury()->Health);
                } else {
                    $obj->HealthAdjusted    = $obj->HealthAdjusted + ($item->Injury()->Health * $item->Num);    
                }
                
                $obj->CastAdjusted      = $obj->CastAdjusted + ($item->Injury()->Cast * $item->Num);                   
            }
         
        }
    } 
    
    
    protected function adjustedStatsMarks(&$obj) {
        if($obj->MarkID >= 1 ) {
            $obj->MoveAdjusted      = $obj->MoveAdjusted + $obj->Mark()->Move;
            $obj->FightAdjusted     = $obj->FightAdjusted + $obj->Mark()->Fight;
            $obj->ShootAdjusted     = $obj->ShootAdjusted + $obj->Mark()->Shoot;
            $obj->ArmourAdjusted    = $obj->ArmourAdjusted + $obj->Mark()->Armour;
            $obj->WillAdjusted      = $obj->WillAdjusted + $obj->Mark()->Will;
            $obj->HealthAdjusted    = $obj->HealthAdjusted + $obj->Mark()->Health;
            $obj->CastAdjusted      = $obj->CastAdjusted + $obj->Mark()->Cast;                
        }
    }     
    
    protected function adjustedStatsItems(&$obj) {
        foreach($obj->itemsAssigned() as $item) {
            if($item->Equipped == 1) {
                $obj->MoveAdjusted      = $obj->MoveAdjusted + $item->Item()->Move;
                $obj->FightAdjusted     = $obj->FightAdjusted + $item->Item()->Fight;
                $obj->ShootAdjusted     = $obj->ShootAdjusted + $item->Item()->Shoot;
                $obj->ArmourAdjusted    = $obj->ArmourAdjusted + $item->Item()->Armour;
                $obj->WillAdjusted      = $obj->WillAdjusted + $item->Item()->Will;
                $obj->HealthAdjusted    = $obj->HealthAdjusted + $item->Item()->Health;
                $obj->CastAdjusted      = $obj->CastAdjusted + $item->Item()->Cast;                
            }
        }
        if($this->hasItemTypeEquipped('dagger') && $this->hasItemTypeEquipped('hand-weapon')) {
            $obj->FightAdjusted     = $obj->FightAdjusted + 1;
        }
    }  
    
    protected function adjustedStatsBeastcrafter(&$obj) {
        foreach($obj->Warband()->BeastcrafterTrait() as $item) {
            $obj->MoveAdjusted      = $obj->MoveAdjusted + $item->Move;
            $obj->FightAdjusted     = $obj->FightAdjusted + $item->Fight;
            $obj->ShootAdjusted     = $obj->ShootAdjusted + $item->Shoot;
            $obj->ArmourAdjusted    = $obj->ArmourAdjusted + $item->Armour;
            $obj->WillAdjusted      = $obj->WillAdjusted + $item->Will;
            $obj->HealthAdjusted    = $obj->HealthAdjusted + $item->Health;
            $obj->CastAdjusted      = $obj->CastAdjusted + $item->Cast;                

        }
    }    
    
    protected function adjustedStatsTraits(&$obj) {
        foreach($obj->Warband()->BeastcrafterTrait() as $item) {
            $obj->MoveAdjusted      = $obj->MoveAdjusted + $obj->Trait()->Move;
            $obj->FightAdjusted     = $obj->FightAdjusted + $obj->Trait()->Fight;
            $obj->ShootAdjusted     = $obj->ShootAdjusted + $obj->Trait()->Shoot;
            $obj->ArmourAdjusted    = $obj->ArmourAdjusted + $obj->Trait()->Armour;
            $obj->WillAdjusted      = $obj->WillAdjusted + $obj->Trait()->Will;
            $obj->HealthAdjusted    = $obj->HealthAdjusted + $obj->Trait()->Health;
            $obj->CastAdjusted      = $obj->CastAdjusted + $obj->Trait()->Cast;                

        }
    }  

    protected function hasItemTypeEquipped($type = 'general') {
        foreach($this->itemsAssigned() as $item) {
            if($item->Item()->ItemClass == $type && $item->Equipped == 1) {
                return true;
            }
        }
        return false;
    }
    
    public function isDuelWeapon() {
        if($this->hasItemTypeEquipped('dagger') && $this->hasItemTypeEquipped('hand-weapon')) {
            return true;
        }    
        return false;
    }    
    
    public function captainTricks() {
        if($this->Type == 'Captain') {
            return $this->Warband()->CaptainTricks()->filter('TrickID:GreaterThan',0);
        }
        return false;
    }  
    
    public function isDemonHunter() {
        if($this->Unit()->ID == 76) {
            return true;
        }
        return false;
    }
    
    /**
     * Can this unit have knightly order traits
     */
    public function isKnightly() {
        // with captains
        //if(in_array($this->Unit()->ID,array(19,25,4,5,6,7,8))) {
            
        // without captains    
        if(in_array($this->Unit()->ID,array(19,25))) {
            return true;
        }
        return false;
    }    
    
    public function isMount() {
        if($this->Unit()->Class == 'Mount') {
            return true;
        }
        return false;
    }    
}