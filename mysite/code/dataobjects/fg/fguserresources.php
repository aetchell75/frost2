<?php

class fguserresources extends DataObject {

    private static $singular_name = 'User Base Resource';

    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Resource'=> 'fgbaseresource'
    );
}
