<?php

class fguserinjuries extends DataObject {

    private static $singular_name = 'User Injuries';
    private static $db = array(
        'Num' => 'Int',
        'Treated' => 'Boolean'
    );
    private static $has_one = array(
        'Unit' => 'fguserunits',
        'Injury'    => 'fginjury'
    );
    static $defaults = array(
        'Num' => 0,
        'Treated' => 0
    );

}
