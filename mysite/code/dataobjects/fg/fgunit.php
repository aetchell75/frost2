<?php

class fgunit extends DataObject {

    private static $singular_name = 'Frostgrave Unit';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Type' => "ENUM('Wizard,Apprentice,Soldier,Monster,Captain,Mount')",
        'Class' => "ENUM('Animal,Sapien,Construct,Demon,Undead,Mount','Sapien')",
        'Summon' => "Boolean",
        'Move' => 'Int',
        'Fight' => 'Int',
        'Shoot' => 'Int',
        'Armour' => 'Int',
        'Will' => 'Int',
        'Health' => 'Int',
        'DamageF' => 'Int',
        'DamageS' => 'Int',
        'Cost' => 'Int',
        'Gear' => 'Text',
        'Notes' => 'Text',
        'Healer' => 'Boolean',
        'Items' => 'Int',
        'Sort' => 'Int',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    static $defaults = array(
        'Summon' => null,
        'Move' => 6,
        'Fight' => 0,
        'Shoot' => 0,
        'Armour' => 10,
        'Will' => 0,
        'Health' => 10,
        'DamageF' => 0,
        'DamageS' => 0,
        'Cost' => null,
        'Healer' => 0,
        'Items' => 1,
        'Sort' => 10,
        'Source' => 'Core',
        'Type' => 'Soldier',
        'Class' => 'Sapien'
    );
    private static $summary_fields = array(
        'Name' => 'Name',
        'Type' => 'Type',
        'Move' => 'Move',
        'Fight' => 'Fight',
        'Shoot' => 'Shoot',
        'Armour' => 'Armour',
        'Will' => 'Will',
        'Health' => 'Health',
        'DamageF' => 'Damage (F)',
        'DamageS' => 'Damage (S)',
        'Cost' => 'Cost',
        'Items' => 'Items',
        'Notes' => 'Notes',
        'Gear' => 'Gear',
        'Source' => 'Source'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        return $fields;
    }

    public function getMoveInt() {
        return sprintf('%0.1f', $this->Move);
    }
}
