<?php

class fguserwarbandlog extends DataObject {

    private static $singular_name = 'Warband Log';
    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Spell'   => 'fgspell',
        'Unit'   => 'fgunit',
        'Injury'   => 'fginjury',
        'Base'   => 'fgbasetype',
        'Resource' => 'fgbaseresource',
        'Item'  => 'fgitem',
        'Level' => 'fguserwizardlevels',

    );   
    private static $db =array(
        'Note'  => 'Text',
        'FundsID' => 'Int',
        'ExpID' => 'Int',
        'CexpID' => 'Int',
    );
    
    static $defaults = array(
        'Spell'   => 0,
        'Unit'   => 0,
        'Injury'   => 0,
        'Base'   => 0,
        'Resource' => 0,
        'Item'  => 0,
        'Level' => 0
    );
}
