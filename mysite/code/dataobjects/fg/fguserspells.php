<?php

class fguserspells extends DataObject {

    private static $singular_name = 'User Spells';
    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Spell' => 'fgspell'
    );
    private static $summary_fields = array(
        'Spell.Name' => 'Name',
        'Spell.School.Name' => 'School',
        'Spell.Diff' => 'Difficulty',
    );

    public function alignmentMod() {
        $id = (int) $this->Spell()->School()->ID;
        $mod = 0;
        if (
                $id == (int) $this->Warband()->School()->Aligned_1 
                || $id == (int) $this->Warband()->School()->Aligned_2 
                || $id == (int) $this->Warband()->School()->Aligned_3
        ) {
            $mod = 2;
        }

        if (
                $id == (int) $this->Warband()->School()->Neutral_1 
                || $id == (int) $this->Warband()->School()->Neutral_2 
                || $id == (int) $this->Warband()->School()->Neutral_3 
                || $id == (int) $this->Warband()->School()->Neutral_4 
                || $id == (int) $this->Warband()->School()->Neutral_5
                || $id == (int) $this->Warband()->School()->Neutral_6
                || $id == (int) $this->Warband()->School()->Neutral_7
                || $id == (int) $this->Warband()->School()->Neutral_8
                || $id == (int) $this->Warband()->School()->Neutral_9
        ) {
            $mod = 4;
        }


        if ($id == (int) $this->Warband()->School()->Opposed_1) {
            $mod = 6;
        }
        return ((int) $this->Spell()->Diff + $mod);
    }
    
    public function wizardDiff() {
        $base = (int)$this->alignmentMod() + (int)$this->Warband()->getWizard()->CastAdjusted;
        $levels = $this->Warband()->WizardLevels()->filter('SpellID',array($this->SpellID));
        foreach($levels as $level) {
            $base = $base - $level->Cast;
        }
        return $base;
    }
    
    public function apprenticeDiff() {
        if($this->Warband()->hasApprentice()){
            $base = (int)$this->alignmentMod() + (int)$this->Warband()->getApprentice()->CastAdjusted + 2;
            $levels = $this->Warband()->WizardLevels()->filter('SpellID',array($this->SpellID));
            foreach($levels as $level) {
                $base = $base - $level->Cast;
            }
            return $base;              
        }
      
    }

}
