<?php

class fgitem extends DataObject {

    private static $singular_name = 'Frostgrave Items';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Desc' => 'Text',
        'Type' => "enum('ivory-scroll,scroll,scroll-distinct,grimoire,weapon,armour,potion,item,captain')",
        'ItemClass' => "enum('general,hand-weapon,dagger,two-handed','general')",
        'Use' => "enum('single,per_game,unlimited')",
        'Cost' => 'Int',
        'Sell' => 'Decimal',
        'Move' => 'Decimal',
        'Fight' => 'Int',
        'Shoot' => 'Int',
        'Armour' => 'Int',
        'Will' => 'Int',
        'Health' => 'Int',
        'DamageF' => 'Int',
        'DamageS' => 'Int',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    private static $has_one = array(
        'Spell' => 'fgspell'
    );
    static $defaults = array(
        'Type' => 'weapon',
        'ItemClass' => 'general',
        'Use' => 'unlimited',
        'Cost' => 0,
        'Sell' => 0.5,
        'Move' => 0,
        'Fight' => 0,
        'Shoot' => 0,
        'Armour' => 0,
        'Will' => 0,
        'Health' => 0,
        'DamageF' => 0,
        'DamageS' => 0,
        'Source' => 'Core'
    );
    private static $summary_fields = array(
        'Name' => 'Item',
        'Type' => 'Type',
        'ItemClass' => 'ItemClass',
        //'Desc' => 'Desc',
        'Use' => 'Use',
        'Cost' => 'Cost',
        'Move' => 'Move',
        'Fight' => 'Fight',
        'Shoot' => 'Shoot',
        'Armour' => 'Armour',
        'Will' => 'Will',
        'Health' => 'Health',
        'DamageF' => 'Damage (F)',
        'DamageS' => 'Damage (S)',
        'Source' => 'Source'
    );

    public function salePrice() {
        if($this->Cost > 0) {
            return ceil($this->Cost * $this->Sell);    
        }
        return $this->Sell;
    }

    public function Icon() {
        switch ($this->Type) {
            case 'weapon':
                $r = 'ra-sword';
                break;
            case 'armour':
                $r = 'ra-knight-helmet';
                break;
            case 'ivory-scroll':
            case 'scroll':
            case 'scroll-distinct':
                $r = 'ra-scroll-unfurled';
                break;
            case 'grimoire':
                $r = 'ra-book';
                break;
            case 'potion':
                $r = 'ra-round-bottom-flask';
                break;
            case 'item':
                $r = 'ra-slash-ring';
                break;
            case 'captain':
                $r = 'ra-castle-flag';
                break;                
            default:
                $r = 'ra-trophy';
                break;
        }
        return $r;
    }

}
