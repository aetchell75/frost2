<?php

class fgusertricks extends DataObject {

    private static $singular_name = 'User Tricks of the Trade';
    private static $db = array(
        'Will'      => 'Int',
        'Health'      => 'Int',
        'Used'      => 'Boolean',
        'CountAsLevel' => 'Boolean',
    );
    private static $has_one = array(
        'Warband' => 'fguserwarband',
        'Trick' => 'fgcaptaintricks'
    );
    
    static $defaults = array(
        'Used' => 0,
        'CountAsLevel' => 1,
    );

}
