<?php 
class fgsacrifice extends fgbeastcraftertraits {
    private static $singular_name = 'Frostgrave Demonic Sacrifice';
    private static $db = array(
        'Note' => 'Text'
    );

    private static $summary_fields = array(
        'Name' => 'Name',
        'Class' => 'Class',
        'Move' => 'Move',
        'Fight' => 'Fight',
        'Shoot' => 'Shoot',
        'Armour' => 'Armour',
        'Will' => 'Will',
        'Health' => 'Health',
        'Note' => 'Note',        
        
    );    
}