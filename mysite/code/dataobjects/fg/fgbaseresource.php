<?php

class fgbaseresource extends DataObject {

    private static $singular_name = 'Frostgrave Base Resources';
    private static $db = array(
        'Name' => 'varchar(255)',
        'Cost' => 'Int',
        'Desc' => 'Text',
        'Source' => "ENUM('Core,Lich Lord,Breeding Pits,Forgotten Pacts,Sellsword,Dark Alchemy,Arcane Locations,Scenario,Spellcaster')"
    );
    
    static $defaults = array(
        'Source' => 'Core'
    );    

}
