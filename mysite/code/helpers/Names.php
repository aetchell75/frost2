<?php

class Names {

    public static function get_random_name() {
        $csvFile = file(dirname(__FILE__).'/names.csv');
        $data = array();
        foreach ($csvFile as $line) {
            $data[] = str_getcsv($line);
        }
        return $data[mt_rand(0, count($data) - 1)][0];
    } 
    
    public static function get_random_cowboyname() {
        $csvFile = file(dirname(__FILE__).'/cowboynames.csv');
        $data = array();
        foreach ($csvFile as $line) {
            $data[] = str_getcsv($line);
        }
        return $data[mt_rand(0, count($data) - 1)][0];
    }     
}
