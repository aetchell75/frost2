<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Helpers implements TemplateGlobalProvider {
    public static function get_template_global_variables() {
        return array(
            // FG
            'SchoolsList',
            'SoldierList',
            'SummonedList',
            'CaptainList',
            'MonsterList',
            'InjuryList',
            'SpellList',
            'StartingSpells',
            'getGrimoires',
            'getScrolls',
            'getDistinctScrolls',
            'BaseList',
            'ResourceList',
            'cssClass',
            'AllUnitsList',
            'AnimalMutations',
            'ConstructMutations',
            'MarksList',
            'BoonList',
            'SacrificeList',
            //DMH
            'DMHFactionsList',
        );
    }

    public static function BoonList() {
        return fgboon::get()->sort('Name');      
    }
	
    public static function SacrificeList() {
        return fgsacrifice::get()->sort('Name');      
    }	
    
    public static function MarksList() {
        return fgmark::get()->sort('Type, Name');      
    }
    
    public static function SchoolsList() {
        return fgschool::get()->filter('Type',array('Primary'))->sort('Name');      
    } 
    
    public static function AnimalMutations() {
        return fgtraits::get()->filter('Class',array('Animal'))->sort('Name');      
    }
	
    public static function ConstructMutations() {
        return fgtraits::get()->filter('Class',array('Construct'))->sort('Name');      
    }	
    
    public static function SpellList() {
        return fgspell::get()->sort('Name');      
    } 
    
    public static function StartingSpells() {
        $spells = fgspell::get()->filter(array('StartingSpell'=>'Y'))->sort('SchoolID DESC,Name');
        return $spells;
    }
    
    public static function getGrimoires() {
        $arrData = array();
        $spells =  fgspell::get()->sort('SchoolID ASC, Name DESC'); 
        $grimoire = fgitem::get()->filter(array('Type' => 'grimoire'))->first();
        foreach($spells as $spell) {
            $arrData[] = new ArrayData(array(
                'Spell' => $spell,
                'Grimoire' => $grimoire
            ));
        }
        return new ArrayList($arrData);
    }
    
    public static function getScrolls() {
        $arrData = array();
        $spells =  fgspell::get(); 
        $grimoire = fgitem::get()->filter(array('Type' => 'scroll'))->first();
        foreach($spells as $spell) {
            if($spell->StartingSpell == 'Y' && $spell->Type != 'Reaction') {
                $arrData[] = new ArrayData(array(
                    'Spell' => $spell,
                    'Grimoire' => $grimoire
                ));                
            }
        }
        return new ArrayList($arrData);
    }  
    
    public static function getDistinctScrolls() {
        $scrolls = fgitem::get()->filter(array('Type' => 'scroll-distinct'));
        $arrData = array();
        foreach($scrolls as $scroll) {
            $arrData[] = new ArrayData(array(
                'Spell' => $scroll->Spell(),
                'Grimoire' => $scroll
            ));                
        }
        return new ArrayList($arrData);        
    }
    
    public static function SoldierList() {
        return fgunit::get()->filter('Type',array('soldier','mount'));
    }
    
    public static function MonsterList() {
        return fgunit::get()->filter('Type',array('monster'));
    }    
    
    public static function AllUnitsList() {
        return fgunit::get();
    }       
    
    public static function SummonedList() {
        return fgunit::get()
                ->filter('Type', array('monster'))
                ->filter('Summon',array(1));
    }    
    
    public static function CaptainList() {
        return fgunit::get()->filter('Type',array('captain'));
    }   
    
    public static function BaseList() {
        return fgbasetype::get();
    }
    
    public static function InjuryList() {
        return fginjury::get();
    }  

    public static function ResourceList() {
        return fgbaseresource::get();
    }     
    
    public static function cssClass($str = false) {
        if($str) {
            return strtolower($str);
        }
    }
    
    
    
    
    // DMH
    public static function DMHFactionsList() {
        return dmhfaction::get()->filter('Primary',1)->sort('Name DESC');
    }
}

