<?php

class Page extends SiteTree {

    private static $db = array(
    );
    private static $has_one = array(
    );

}

class Page_Controller extends ContentController {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
        // You can include any CSS or JS required by your project here.
        // See: http://doc.silverstripe.org/framework/en/reference/requirements
        Requirements::set_force_js_to_bottom(true);
    }

    /**
     *
     * @return boolean|\ArrayData
     * Add this to Page.ss
     *
      <% if StatusMessage %>
      <% control StatusMessage %>
      <div class="message-$Status">
      $Message
      </div>
      <% end_control %>
      <% end_if %>
     *
     *
     * how to set a message
      Session::set('ActionStatus', 'success');
      Session::set('ActionMessage', 'Profile Saved');
     */
    public function StatusMessage() {
        if (Session::get('ActionMessage')) {
            $message = Session::get('ActionMessage');
            $status = Session::get('ActionStatus');

            Session::clear('ActionStatus');
            Session::clear('ActionMessage');

            return new ArrayData(array('Message' => $message, 'Status' => $status));
        }

        return false;
    }

    public function accountStatus() {
        if (Member::currentUser()) {
            return Member::currentUser()->AccountType;
        }
        return 'ANON';
    }

	public function accountIdent() {
        if (Member::currentUser()) {
            return 'U-'.Member::currentUser()->ID;
        }
        return 'U-0';
    }
	
    public function showGA() {
        if (Permission::checkMember(Member::currentUser(), 'CMS_ACCESS')) {
            //user can access the CMS
            return false;
        }
        return true;
    }

    public function warbandList($limit = false) {
        if (Member::currentUser() && Member::currentUser()->ID) {
            if ($this->accountStatus() == 'FREE') {
                return fguserwarband::get()->filter(array('MemberID' => Member::currentUser()->ID))->first();
            } else {
                if ($limit == false || (int) $limit <= 0) {
                    $limit = 10;
                } else {
                    $limit = (int)$limit;
                            
                }
                return fguserwarband::get()->filter(array('MemberID' => Member::currentUser()->ID))->limit($limit);
            }
        } else {
            return false;
        }
    }

}
