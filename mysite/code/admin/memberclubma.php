<?php
class memberclubma extends ModelAdmin {
  
    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'MemberClub'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'memberclubma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Members Clubs';   
}