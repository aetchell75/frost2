<?php
class dmhfactionrulema extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'dmhfactionrule'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'dmhfactionrule';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'DMH Faction rules';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}