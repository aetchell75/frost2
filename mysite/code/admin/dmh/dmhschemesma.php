<?php
class dmhschemesma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'dmhschemes'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'dmhschemes';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'DMH Schemes';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}