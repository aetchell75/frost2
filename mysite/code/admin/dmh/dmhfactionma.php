<?php
class dmhfactionma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'dmhfaction'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'dmhfaction';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'DMH Factions';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}