<?php
class dmhweaponma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'dmhweapon'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'dmhweapon';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'DMH Weapons';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}