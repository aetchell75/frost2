<?php
class dmhunitma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'dmhunit'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'dmhunit';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'DMH Units';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}