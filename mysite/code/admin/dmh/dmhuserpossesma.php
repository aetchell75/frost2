<?php
class dmhuserpossesma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'dmhuserposse'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'dmhuserposses';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'DMH USer Posses';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}