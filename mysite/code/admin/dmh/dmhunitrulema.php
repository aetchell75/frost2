<?php
class dmhunitrulema extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'dmhunitrule'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'dmhunitrule';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'DMH Unit Rules';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}