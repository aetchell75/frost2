<?php
class fgtraitsma extends ModelAdmin {
  
  //private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgtraits'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgtraitsma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Traits';   
}