<?php
class fgbeastcrafterlevelsma extends ModelAdmin {
  
  //private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgbeastcrafterlevels'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgbeastcrafterlevelssma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Beastcrafter Levels';   
}