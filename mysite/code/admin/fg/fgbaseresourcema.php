<?php
class fgbaseresourcema extends ModelAdmin {
  
  //private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgbaseresource'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgbaseresourcema';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Base Resource';   
}