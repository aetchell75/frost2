<?php
class fgspellsma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgspell'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgspellsma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Spells';     
}