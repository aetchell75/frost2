<?php
class fgwarbandma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fguserwarband'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgwarbandma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Warbands';   
}