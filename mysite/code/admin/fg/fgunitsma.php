<?php
class fgunitsma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgunit'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgunitsma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'units';    
}