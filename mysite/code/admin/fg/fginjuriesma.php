<?php
class fginjuriesma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fginjury'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fginjuriesma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Injuries';
    
   private static $model_importers = array(
      'fginjury' => 'CsvBulkLoader',
   );       
}