<?php
class fgboonma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgboon'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgboonma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Demonic Boons';   
}