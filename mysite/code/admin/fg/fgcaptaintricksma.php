<?php
class fgcaptaintricksma extends ModelAdmin {
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgcaptaintricks'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgcaptaintricksma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Tricks of the Trade';   
}