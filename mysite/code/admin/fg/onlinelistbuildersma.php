<?php
class onlinelistbuildersma extends ModelAdmin {
  
  //private static $menu_icon = "mysite/images/menu-icons/dice.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'OnlineListBuilders'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'onlinelistbuildersma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Online List Builders';   
}