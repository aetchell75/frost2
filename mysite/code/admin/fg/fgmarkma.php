<?php
class fgmarkma extends ModelAdmin {
  
  //private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgmark'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgmarkma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Marks';   
}