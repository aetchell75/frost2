<?php
class fgschoolsma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgschool'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgschools';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Schools of Magic';
    
   private static $model_importers = array(
      'fgschool' => 'CsvBulkLoader',
   );      
}