<?php
class fgitemsma extends ModelAdmin {
  
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgitem'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgitemsma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Items';
    
   private static $model_importers = array(
      'fgitem' => 'CsvBulkLoader',
   );     
}