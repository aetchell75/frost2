<?php
class fgbeastcraftertraitsma extends ModelAdmin {
  
  //private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgbeastcraftertraits'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgbeastcraftertraitsma';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Beastcrafter Traits';   
}