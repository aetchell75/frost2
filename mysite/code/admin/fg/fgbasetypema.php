<?php
class fgbasetypema extends ModelAdmin {
//private static $menu_icon = "mysite/images/menu-icons/frostgrave.png";

    /**
     * 
     * @var array 
     */
    private static $managed_models = array(
        'fgbasetype'
    );
    
    /**
     *
     * @var string 
     */
    private static $url_segment = 'fgbasetype';
    
    /**
     *
     * @var string 
     */    
    private static $menu_title = 'Base Type';   
}