<?php

class dmhposse extends GamingApp {

    private static $db = array(
        'HelpText' => 'HTMLText'
    );
    private static $has_one = array(
    );
}

class dmhposse_controller extends GamingApp_Controller {

    private static $url_handlers = array(
        'print/$ID' => 'printfriendly',
        'public/$ID' => 'publicfriendly'
    );
    private static $allowed_actions = array(
        'view',
        'delete',
        'unitbuy',
        'unitdismiss',
        'reloadposse',
        'setweapon',
    );

    public function init() {
        parent::init();
        Requirements::javascript('mysite/js/dmh/posse.js');
        if (!Member::currentUserID()) {
            $this->redirect('/');
        }
        $this->posse = $this->loadPosse();
            Requirements::css('themes/' . SSViewer::current_theme() . "/css/dmh.css");
    }

    protected function loadPosse() {
        $posse = DataObject::get_one('dmhuserposse', array(
                    'hash' => $this->getRequest()->param('ID')
        ));
            if (!$posse || $posse->MemberID != Member::currentUser()->ID) {
                return $this->httpError(404, 'You do not own the posse you are trying to view. The Thought Police have been notified of your infraction.');
            }

        if ($posse) {
            return $posse;
        }
    }
    
    public function unitsForFaction() {
        return dmhunit::get()->filter('FactionID',$this->posse->FactionID);
    }
    
    public function unitsForRogue() {
        return dmhunit::get()->filter('FactionID',15);
    }    
    
    public function unitsForAligned() {
        if($this->posse->Faction()->Align == 'Good') {
            $id = 14;
        } else {
            $id = 13;
        }
        return dmhunit::get()->filter('FactionID',$id);
    }        
    
    public function rulesForFaction() {
        return $this->posse->Faction()->Rules();
        //return dmhfactionrule::get()->filter('FactionID',$this->posse->FactionID);
    }    

    public function reloadposse(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                return $this->customise(new ArrayData(array(
                            'Posse' => $this->posse
                        )))->renderWith('possescreenview');
            }
        }
    }
    
    public function unitdismiss(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $unit = DataObject::get_by_id('dmhusermember', (int) $request->postVar('id'));
                $unit->delete();
            }
        }
    }

    public function unitbuy(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->unitadd($request);
            }
        }
    }
    
    public function setweapon(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $item = DataObject::get_by_id('dmhusermember', (int) $request->postVar('id'));
                $item->SelectedWeaponID = (int) $request->postVar('weapon');
                $item->write();
                return 'unitID:'.$request->postVar('id').' / weaponID:'.$request->postVar('weapon');
            }
        }        
    }

    protected function unitadd(SS_HTTPRequest $request) {
            $unit = DataObject::get_by_id('dmhunit', (int) $request->postVar('id'));
            $cost = (int) $request->postVar('cost');

            $dmhunit = new dmhusermember();
            $dmhunit->PosseID = $this->posse->ID;
            $dmhunit->UnitID = (int) $request->postVar('id');
            $dmhunit->Rep = $cost;
            $dmhunit->Name = Names::get_random_cowboyname();
            $dmhunit->SelectedWeaponID = $unit->WeaponList()->first()->ID;
            $updatedUnit = $dmhunit->write();
    }    
    
    
    
    
    
    public function view() {
        if ($this->getRequest()->param('ID')) {
            // build the warband here for display in the controlling page object
            return array(
                'Posse' => $this->posse,
                'Title' => $this->posse->GangName
            );
        }
    }   
    
    public function delete() {
        if ($this->getRequest()->param('ID')) {
            $posse = $this->loadPosse();	
            if ($posse->GangMembers()->Count() > 0) {
                foreach ($posse->GangMembers() as $e) {
                    $e->delete();
                }
            }				
            $posse->delete();
            $this->redirect($this->Parent->Link());
        }
    }    
    
    public function RepTotal() {
        $total = 0;
        foreach($this->posse->GangMembers() as $gm) {
            $total = $total + $gm->Unit()->Rep;
        }
        return $total;
    }
    
    public function RepRemainTotal() {
        return $this->posse->Rep - $this->RepTotal();
    }

}
