<?php

class dmhposseholder extends GamingApp {

    private static $db = array(
    );
    private static $has_one = array(
    );

}

class dmhposseholder_controller extends GamingApp_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'addposse'
    );

    public function init() {
        parent::init();
        Requirements::javascript('mysite/js/dmh/posse.js');
        Requirements::css('themes/'.SSViewer::current_theme()."/css/dmh.css");         
    }

    public function posseList($limit = false) {
        if (Member::currentUser() && Member::currentUser()->ID) {
            if($this->accountStatus() == 'FREE') {
                return dmhuserposse::get()->filter(array('MemberID' => Member::currentUser()->ID))->first();
            } else {
                return dmhuserposse::get()->filter(array('MemberID' => Member::currentUser()->ID));    
            }
        } else {
            return false;
        }
    }

    public function NumPosse() {
        if (Member::currentUser() && Member::currentUser()->ID) {
            return dmhuserposse::get()->filter(array('MemberID' => Member::currentUser()->ID))->Count();
        } else {
            return 0;
        }
    }
    public function canCreatePosse() {
        /*
         * Check if this users ID has an associated clubID
         * clubs can add a number of members and removes the warband limit
         */

        /*
         * If not in a club, then check the individual user level
         */
        if (
                $this->accountStatus() == 'FREE' && $this->NumPosse() >= 1
        ) {
            return false;
        }
        return true;
    }

    public function addposse() {
        if ($this->canCreatePosse() == true) {
            //$this->request->postVars();
            $posse = new dmhuserposse();
            $posse->Rep = 21;
            $posse->Fame = 0;
            $posse->MemberID = Member::currentUser()->ID;
            $posse->GangName = $this->request->postVar('GangName');
            $posse->FactionID = $this->request->postVar('Faction');
            $posseID = $posse->write();

            $this->redirectBack($this->Link() . '?posseCreated=1');
        }
        $this->redirectBack($this->Link() . '?maxreached=1');
    }
}
