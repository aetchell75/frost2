<?php

class Club extends Page {

    private static $db = array(
    );
    private static $has_one = array(
    );

}

class Club_Controller extends Page_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'clublist',
        'club'
    );

    public function init() {
        parent::init();


        // initialise a map
        $config = Config::inst()->get('GoogleMapField', 'default_options');
        //print_r($config);
        $gmapsParams = array(
            'callback' => 'initMap',
        );
        if ($key = $config['api_key']) {
            $gmapsParams['key'] = $key;
        }
        Requirements::javascript('mysite/js/Club.js');
        Requirements::javascript('//maps.googleapis.com/maps/api/js?' . http_build_query($gmapsParams));
        // You can include any CSS or JS required by your project here.
        // See: http://doc.silverstripe.org/framework/en/reference/requirements
    }

    public function Map() {
        
    }
    
    public function club() {
        if ($this->getRequest()->param('ID')) {
           // fetch a club based on ID
            $Club = DataObject::get_by_id('MemberClub',$this->getRequest()->param('ID'));
            //print_r($Club);
            return array(
                'Club' => $Club,
            );            
        }
    }    

    public function Clubs() {
        return MemberClub::get()->sort('Name');
    }

    public function ClubsJson() {
        $clubs = $this->Clubs();
        if ($clubs) {
            $json = array();
            foreach ($clubs as $club) {
                $json[] = array(
                    'ID' => $club->ID,
                    'Name' => $club->Name,
                    'ContactName' => $club->ContactName,
                    'ContactEmail' => $club->ContactEmail,
                    'ContactNumber' => $club->ContactNumber,
                    'About' => $club->About,
                    'Fees' => $club->Fees,
                    'Rules' => $club->Rules,
                    'Meetings' => $club->Meetings,
                    'Address' => $club->Address,
                    'Meetings' => $club->Meetings,
                    'Website' => $club->Website,
                    'Facebook' => $club->Facebook,
                    'Twitter' => $club->Twitter,
                    'Google' => $club->Google,
                    'Lat' => (float) $club->Lat,
                    'Lng' => (float) $club->Lng,
                );
            }
            return json_encode($json);
        }
        return false;
    }

    public function clublist(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            return $this->ClubsJson();
        }
        echo '';
    }

    public function itembuy(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                if (($this->warband->Funds - (int) $request->postVar('cost')) >= 0) {
                    $fguseritems = new fguseritems();
                    $fguseritems->WarbandID = $this->warband->ID;
                    $fguseritems->ItemID = (int) $request->postVar('id');
                    if ($request->postVar('spell')) {
                        $fguseritems->SpellID = (int) $request->postVar('spell');
                    }
                    $updatedItem = $fguseritems->write();
                    if ((int) $request->postVar('cost') > 0) {
                        $this->warband->Funds = $this->warband->Funds - (int) $request->postVar('cost');
                        $this->warband->write();
                        $this->warband->logEvent('Item', (int) $request->postVar('id'), 'purchased');
                    }
                }
            }
        }
    }

}
