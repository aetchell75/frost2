<?php

class MyProfile extends Page {

    private static $db = array(
        'HelpText' => 'HTMLText'
    );

}

class MyProfile_Controller extends Page_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'ClubForm'
    );

    public function init() {
        parent::init();
        if (!Member::currentUserID()) {
            $this->redirect('/');
        } else {
            // if (Member::currentUser()->AccountType != 'PAID') {
            //$this->redirect('/');
            // } else {
            $this->member = Member::currentUser();
            // }
        }
    }

    public function ClubForm() {

        $club = $this->member->Club();
        $member = $this->member;
        $Public = new CheckboxField('Public', 'Make my club page public');
        $Public->setValue((int) $member->Club()->Public);
        $Name = new TextField('Name', 'Club Name');
        $Name->setDescription('Enter your clubs name.');
        $Name->setValue($member->Club()->Name);


        $ContactName = new TextField('ContactName', 'Contact Name');
        $ContactName->setDescription('Who should potential members contact.');
        $ContactName->setValue($member->Club()->ContactName);

        $ContactEmail = new TextField('ContactEmail', 'Contact Email');
        $ContactEmail->setDescription('Optional. Note that we CANNOT protect you against spam if you enter an email addresss.');
        $ContactEmail->setValue($member->Club()->ContactEmail);

        $ContactNumber = new TextField('ContactNumber', 'Contact Number');
        $ContactNumber->setDescription('Optional mobile/cell or landland for the contact.');
        $ContactNumber->setValue($member->Club()->ContactNumber);

        $About = new TextareaField('About', 'About');
        $About->setDescription('Tell us about your club, its history, games played etc.');
        $About->setValue($member->Club()->About);

        $Fees = new TextareaField('Fees', 'Club Fees');
        $Fees->setDescription('Enter your clubs fee structure, if any.');
        $Fees->setValue($member->Club()->Fees);

        $Rules = new TextareaField('Rules', 'Club Rules');
        $Rules->setDescription('Does your club have a code of conduct or set of rules for all members to follow?.');
        $Rules->setValue($member->Club()->Rules);

        $Meetings = new TextareaField('Meetings', 'Dates');
        $Meetings->setDescription('How often do you meet?.');
        $Meetings->setValue($member->Club()->Meetings);

        $Website = new TextField('Website', 'Club website');
        $Website->setDescription('Does your club have a website? enter the URL here.');
        $Website->setValue($member->Club()->Website);

        $Facebook = new TextField('Facebook', 'Facebook');
        $Facebook->setDescription('Facebook URL.');
        $Facebook->setValue($member->Club()->Facebook);

        $Twitter = new TextField('Twitter', 'Twitter');
        $Twitter->setDescription('Twitter URL.');
        $Twitter->setValue($member->Club()->Twitter);

        $Google = new TextField('Google', 'Google Page URL');
        $Google->setDescription('Google+ URL.');
        $Google->setValue($member->Club()->Google);

        $Map = new GoogleMapField($member->Club(), 'Location');
        $Map->setDescription('Click in the map or enter an address and press return.');
        $fields = new FieldList(
                $Map, $Name, $ContactName, $ContactEmail, $ContactNumber, $About, $Fees, new LiteralField('sep-1', '<div class="sep"></div>'), $Rules, $Meetings, $Website, $Facebook, $Twitter, $Google
                //$Public
        );

        $actions = new FieldList(
                FormAction::create("doMyClub")->setTitle("Update club")->addExtraClass('btn')->addExtraClass('btn-success')
        );

        $required = new RequiredFields('Name');

        $form = new Form($this, 'ClubForm', $fields, $actions, $required);
        //$form->setTemplate('MyClubForm');
        return $form;
    }

    public function doMyClub($RAW_data, Form $form) {
        $data = Convert::raw2sql($RAW_data);
        $member = Member::currentUser();
        $form->sessionMessage('Club:  ' . $data['Name'] . ' has been added to the database', 'success');
        if (isset($member->Club()->ID)) {
            $club = $member->Club(); //new MemberClub();
        } else {
            $club = new MemberClub();
        }
        //$club = $member->Club();//new MemberClub();
        $member->Club()->MemberID = $member->ID;
        $member->Club()->Name = strip_tags($data['Name']);
        $member->Club()->ContactName = strip_tags($data['ContactName']);
        $member->Club()->ContactEmail = strip_tags($data['ContactEmail']);
        $member->Club()->ContactNumber = strip_tags($data['ContactNumber']);
        $member->Club()->About = strip_tags($data['About']);
        $member->Club()->Rules = strip_tags($data['Rules']);
        $member->Club()->Fees = strip_tags($data['Fees']);
        $member->Club()->Address = strip_tags($data['Search']);
        $member->Club()->Meetings = strip_tags($data['Meetings']);
        $member->Club()->Facebook = strip_tags($data['Facebook']);
        $member->Club()->Twitter = strip_tags($data['Twitter']);
        $member->Club()->Google = strip_tags($data['Google']);
        $member->Club()->Lat = $data['MemberClub_Lat_Lng']['Latitude'];
        $member->Club()->Lng = $data['MemberClub_Lat_Lng']['Longitude'];
        $member->Club()->Public = (isset($data['Public']) ? 1 : 0);
        $id = $member->Club()->write();
        $member->ClubID = $id;
        $member->write();
        return $this->redirectBack();
    }

}
