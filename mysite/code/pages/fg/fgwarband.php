<?php

class fgwarband extends GamingApp {

    private static $db = array(
        'HelpText' => 'HTMLText'
    );
    private static $has_one = array(
    );

}

class fgwarband_controller extends GamingApp_Controller {

    private static $url_handlers = array(
        'print/$ID' => 'printfriendly',
        'public/$ID' => 'publicfriendly'
    );
    private static $allowed_actions = array(
        'delete',
        'view',
        'logs',
        'qrs',
        'help',
        'publicview',
        'printfriendly',
        'reloadwarband',
        'unitbuy',
        'unitdismiss',
        'apprenticebuy',
        'captainbuy',
        'captainupdate',
        'itembuy',
        'itemremove',
        'itemsell',
        'itemassign',
        'itemstore',
        'itemequip',
        'adjustfunds',
        'adjustexp',
        'adjustcexp',
        'setinjured',
        'addinjury',
        'addspell',
        'removeinjury',
        'treatinjury',
        'addlevel',
        'undolevel',
        'removespell',
        'addtrick',
        'removetrick',
        'usetrick',
        'homebase',
        'buyresource',
        'removeresource',
        'renameunit',
        'setillusion',
        'addbeastcraftertrait',
        'removebeastcrafter',
        'removelich',
        'addmutation',
        'addmark',
        'removemark',
        'addpact',
        'removepact',
        'addboon',
        'removeboon',
        'addsacrifice',
        'removesacrifice',
        'addnote',
        'removenote',
        'addmount',
        'removemount',
    );

    public function init() {
        parent::init();
        Requirements::javascript('mysite/js/list.js');
        Requirements::javascript('mysite/js/fg/warband.js');
        if (!Member::currentUserID()) {
            $this->redirect('/');
        }
        $this->warband = $this->loadWarband();
        $this->pageAction = $this->getRequest()->param('Action');
        Requirements::css('themes/' . SSViewer::current_theme() . "/css/fg-class.css");
    }

    protected function loadWarband() {
        $warband = DataObject::get_one('fguserwarband', array(
                    'hash' => $this->getRequest()->param('ID')
        ));
        if (Member::currentUser()->ID !== 2) {
            if (!$warband || $warband->MemberID != Member::currentUser()->ID) {
                return $this->httpError(404, 'You do not own the warband you are trying to view. The Thought Police have been notified of your infraction.');
            }
        }


        if ($warband) {
            return $warband;
        }
    }

    public function reloadwarband(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                return $this->customise(new ArrayData(array(
                            'Warband' => $this->warband
                        )))->renderWith('warbandscreenview');
            }
        }
    }

    public function printfriendly() {
        //if($this->accountStatus() == 'PAID') {
        if ($this->getRequest()->param('ID')) {
            Requirements::javascript('mysite/js/fg/print.js');
            return $this->customise(new ArrayData(array(
                        'Warband' => $this->warband,
                        'Title' => $this->warband->WizardName
                    )))->renderWith('fgwarband_print');
        }
        //} else {
        //return $this->renderWith('donateinfo');
        //}
    }

    public function logs() {
        if ($this->getRequest()->param('ID')) {
            return array(
                'Warband' => $this->warband,
                'Title' => $this->warband->WizardName
            );
        }
    }

    public function qrs() {
        // if($this->accountStatus() == 'PAID') {
        if ($this->getRequest()->param('ID')) {
            return array(
                'Warband' => $this->warband,
                'Title' => $this->warband->WizardName
            );
        }
        //} else {
        //        return $this->renderWith('donateinfo');
        //}
    }

    public function Level() {
        return $this->warband->getLevel() + $this->warband->getWizard()->LevelAdjusted;
    }

    public function apprenticeCost() {
        return (int) (($this->Level() - 10) * 10) + 300;
    }

    public function help() {
        if ($this->getRequest()->param('ID')) {
            return array(
                'Warband' => $this->warband,
                'Title' => 'Frostgrave Warband Help'
            );
        }
    }

    public function publicview() {
        if ($this->getRequest()->param('ID')) {
            return $this->customise(new ArrayData(array(
                        'Warband' => $this->warband,
                        'Title' => $this->warband->WizardName
                    )))->renderWith('fgwarband_print');
        }
    }

    public function view() {
        if ($this->getRequest()->param('ID')) {
            // build the warband here for display in the controlling page object
            return array(
                'Warband' => $this->warband,
                'Title' => $this->warband->WizardName
            );
        }
    }

    public function delete() {
        if ($this->getRequest()->param('ID')) {
            $warband = $this->loadWarband();
            if ($warband->Events()->Count() > 0) {
                foreach ($warband->Events() as $e) {
                    $e->delete();
                }
            }

            if ($warband->Events()->Count() > 0) {
                foreach ($warband->Events() as $e) {
                    $e->delete();
                }
            }

            if ($warband->Boons()->Count() > 0) {
                foreach ($warband->Boons() as $e) {
                    $e->delete();
                }
            }

            if ($warband->Sacrifices()->Count() > 0) {
                foreach ($warband->Sacrifices() as $e) {
                    $e->delete();
                }
            }


            if ($warband->BaseResources()->Count() > 0) {
                foreach ($warband->BaseResources() as $e) {
                    $e->delete();
                }
            }
            if ($warband->WizardLevels()->Count() > 0) {
                foreach ($warband->WizardLevels() as $e) {
                    $e->delete();
                }
            }
            if ($warband->CaptainTricks()->Count() > 0) {
                foreach ($warband->CaptainTricks() as $e) {
                    $e->delete();
                }
            }
            if ($warband->Items()->Count() > 0) {
                foreach ($warband->Items() as $e) {
                    $e->delete();
                }
            }

            if ($warband->Spells()->Count() > 0) {
                foreach ($warband->Spells() as $e) {
                    $e->delete();
                }
            }
            if ($warband->WarbandUnits()->Count() > 0) {
                foreach ($warband->WarbandUnits() as $e) {
                    if ($e->Injuries()->Count() > 0) {
                        foreach ($e->Injuries as $injury) {
                            $injury->delete();
                        }
                    }
                    $e->delete();
                }
            }

            $warband->delete();

            $this->redirect($this->Parent->Link());
        }
    }

    public function getItems($type = false) {
        if ($type == false) {
            // get all items
            return fgitem::get();
        }
        // otherwise get by type
        return fgitem::get()->filter(array('Type' => $type));
    }

    public function itembuy(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                if (($this->warband->Funds - (int) $request->postVar('cost')) >= 0) {
                    $fguseritems = new fguseritems();
                    $fguseritems->WarbandID = $this->warband->ID;
                    $fguseritems->ItemID = (int) $request->postVar('id');
                    if ($request->postVar('spell')) {
                        $fguseritems->SpellID = (int) $request->postVar('spell');
                    }
                    $updatedItem = $fguseritems->write();
                    if ((int) $request->postVar('cost') > 0) {
                        $this->warband->Funds = $this->warband->Funds - (int) $request->postVar('cost');
                        $this->warband->write();
                        $this->warband->logEvent('Item', (int) $request->postVar('id'), 'purchased');
                    }
                }
            }
        }
    }

    public function itemassign(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                (int) $request->postVar('item');
                (int) $request->postVar('id');
                $item = DataObject::get_by_id('fguseritems', (int) $request->postVar('item'));
                $unit = DataObject::get_by_id('fguserunits', (int) $request->postVar('id'));
                if ($item) {
                    if ($item->Item()->Type == 'Captain' && $unit->Type != 'Captain') {
                        // dont allow non-captain items to be added to non-captain units
                    } else {
                        $item->AssignedID = (int) $request->postVar('id');
                        $item->write();
                        $this->warband->logEvent('Item', (int) $request->postVar('id'), 'assigned');
                    }
                }
            }
        }
    }

    public function itemremove(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $item = DataObject::get_by_id('fguseritems', (int) $request->postVar('id'));
            if ($item) {
                $item->delete();
                $this->warband->logEvent('Item', (int) $request->postVar('id'), 'deleted');
            }
        }
    }

    public function itemsell(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $item = DataObject::get_by_id('fguseritems', (int) $request->postVar('id'));
            if ($item) {
                $this->warband->Funds = (int) ($this->warband->Funds + $item->Item()->salePrice());
                $item->delete();
                $this->warband->write();
                $this->warband->logEvent('Item', (int) $request->postVar('id'), 'sold');
            }
        }
    }

    public function itemstore(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $item = DataObject::get_by_id('fguseritems', (int) $request->postVar('id'));
            if ($item) {
                $item->AssignedID = 0;
                $item->Equipped = 0;
                $item->write();
                $this->warband->logEvent('Item', (int) $request->postVar('id'), 'stored');
            }
        }
    }

    public function itemequip(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $item = DataObject::get_by_id('fguseritems', (int) $request->postVar('id'));
            if ($item) {
                $item->Equipped = ($item->Equipped == 1 ? 0 : 1);
                $item->write();
                $this->warband->logEvent('Item', (int) $request->postVar('id'), 'equipped');
            }
        }
    }

    public function unitbuy(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->unitadd($request);
            }
        }
    }

    protected function unitadd(SS_HTTPRequest $request) {
        if (($this->warband->Funds - (int) $request->postVar('cost')) >= 0) {
            $unit = DataObject::get_by_id('fgunit', (int) $request->postVar('id'));

            if ($unit->Type == 'Apprentice') {
                // special acton for adding an apprentice, price is based on current exp level
                // make adjustments to price etc here
                $cost = $this->apprenticeCost();
            } else {
                $cost = (int) $request->postVar('cost');
            }

            $fguserunit = new fguserunits();
            $fguserunit->WarbandID = $this->warband->ID;
            $fguserunit->UnitID = (int) $request->postVar('id');
            $fguserunit->Type = $unit->Type;
            $fguserunit->Name = Names::get_random_name();
            $updatedUnit = $fguserunit->write();
            $this->warband->logEvent('Unit', (int) $request->postVar('id'), $unit->Type . ' Purchased');
            if ((int) $request->postVar('cost') > 0) {
                // remove the funds if a cost was associated with this transaction
                $this->warband->Funds = $this->warband->Funds - $cost;
                $this->warband->write();
            }
        }
        //return $this->warband;
    }

    public function addmutation(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $unit = DataObject::get_by_id('fguserunits', (int) $request->postVar('id'));
                $unit->TraitID = (int) $request->postVar('trait');
                $unit->write();
            }
        }
        //return $this->warband;
    }

    public function unitdismiss(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $unit = DataObject::get_by_id('fguserunits', (int) $request->postVar('id'));

                if ($unit->Type == 'Captain') {
                    // delete all his items
                    foreach ($this->warband->Items()->filter('AssignedID', array($unit->ID)) as $item) {
                        $item->delete();
                    }
                    foreach ($this->warband->CaptainTricks() as $trick) {
                        $trick->delete();
                    }
                    $this->warband->CEXP = 0;
                    $this->warband->write();
                } else {
                    // store all his items
                    foreach ($this->warband->Items()->filter('AssignedID', array($unit->ID)) as $item) {
                        $item->AssignedID = 0;
                        $item->write();
                    }
                }

//                foreach($unit->Injuries() as $injury) {
//                    $injury->delete();
//                }

                $unit->delete();
                $this->warband->logEvent('Unit', (int) $request->postVar('id'), $unit->Type . ' Dismissed');
            }
        }
    }

    public function adjustfunds(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->warband->Funds = (int) $request->postVar('val');
                $this->warband->write();
                $this->warband->logEvent('Funds', (int) $request->postVar('val'), 'Adjusted');
            }
        }
    }

    public function adjustexp(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->warband->EXP = (int) $request->postVar('val');
                $this->warband->write();
                $this->warband->logEvent('Exp', (int) $request->postVar('val'), 'Adjusted');
            }
        }
    }

    public function adjustcexp(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->warband->CEXP = (int) $request->postVar('val');
                $this->warband->write();
                $this->warband->logEvent('Cexp', (int) $request->postVar('val'), 'Adjusted');
            }
        }
    }

    public function setinjured(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $unit = DataObject::get_by_id('fguserunits', (int) $request->postVar('id'));
                $unit->Injured = ($unit->Injured == 1 ? 0 : 1);
                $unit->write();
            }
        }
    }

    public function addinjury(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                // check to see if this unit already has one of these injuries
                $injury = DataObject::get('fguserinjuries')
                        ->filter(array('UnitID' => (int) $request->postVar('unit')))
                        ->filter(array('InjuryID' => (int) $request->postVar('id')))
                        ->first();

                if ($injury == false) {
                    $injury = new fguserinjuries();
                }
                if ($injury->Num < $injury->Injury()->Max) {
                    $injury->UnitID = (int) $request->postVar('unit');
                    $injury->InjuryID = (int) $request->postVar('id');
                    $injury->Num = $injury->Num + 1;
                    $injury->write();
                    $this->warband->logEvent('Injury', (int) $request->postVar('val'), 'Added');
                }
            }
        }
    }

    public function removeinjury(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $injury = DataObject::get_by_id('fguserinjuries', (int) $request->postVar('id'));
                if ($injury) {
                    $injury->delete();
                    $this->warband->logEvent('Injury', (int) $request->postVar('val'), 'Removed');
                }
            }
        }
    }

    public function addspell(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->warband->Spells()->filter('SpellID', array((int) $request->postVar('spell')));


                if ($this->warband->Spells()->filter('SpellID', array((int) $request->postVar('spell')))->Count() <= 0) {
                    $addSpell = new fguserspells();
                    $addSpell->SpellID = (int) $request->postVar('spell');
                    $addSpell->WarbandID = $this->warband->ID;
                    $addSpell->write();
                }
            }
        }
    }

    public function treatinjury(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $injury = DataObject::get_by_id('fguserinjuries', (int) $request->postVar('id'));
                if ($injury->Treated == 0) {
                    // looks like this needs treating
                    if ($injury && $injury->Injury()->Type == 'Niggling') {
                        if ($injury->Num >= 2) {
                            $cost = 40 - (int) $this->warband->nigglingDiscount();
                        } else if ($injury->Num < 2) {
                            $cost = 30 - (int) $this->warband->nigglingDiscount();
                        }
                        if ($cost < 0) {
                            $cost = 0;
                        }
                        if ($cost > 0) {
                            $this->warband->Funds = $this->warband->Funds - (int) $cost;
                            $this->warband->write();
                        }
                    }
                }

                $injury->Treated = ($injury->Treated == 1 ? 0 : 1);
                $injury->write();
                $this->warband->logEvent('Injury', (int) $request->postVar('val'), 'Treated');
            }
        }
    }

    public function addlevel(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $level = new fguserwizardlevels();
                $EXP = $this->warband->EXP;
                if ($this->warband->Lich == 1) {
                    $this->warband->EXP = $EXP - 150;
                    $restoreXP = 150;
                } else {
                    $this->warband->EXP = $EXP - 100;
                    $restoreXP = 100;
                }


                if (strtolower($request->postVar('val')) == 'grimoire') {
                    $level->Newspell = 1;
                    $addSpell = new fguserspells();
                    $addSpell->SpellID = (int) $request->postVar('id');
                    $addSpell->WarbandID = $this->warband->ID;
                    $addSpell->write();
                } else if (strstr(strtolower($request->postVar('val')), 'beastcrafter')) {
                    if ((int) (int) $request->postVar('id') > 0) {
                        $level->Cast = 1;
                        $level->SpellID = (int) $request->postVar('id');
                    }
                    $this->warband->BeastcrafterLevelID = (int) $request->postVar('level');
                    $level->BeastCrafter = 1;
                } else if (strtolower($request->postVar('val')) == 'lichfail') {
                    if ((int) $request->postVar('level') == 5) {
                        $level->Health = -1;
                        $this->warband->LevelMod = -8;
                        $level->Level = -1;
                    }
                    if ((int) $request->postVar('level') == 10) {
                        $level->Health = -2;
                        $level->Will = -1;
                        $level->Level = -3;
                    }
                    if ((int) $request->postVar('level') == 15) {
                        $level->Health = -3;
                        $level->Will = -2;
                        $level->Level = -5;
                    }
                    if ((int) $request->postVar('level') == 20) {
                        $level->Health = -6;
                        $level->Will = -2;
                        $level->Level = -8;
                    }
                    $this->warband->Lich = 0;
                    $this->warband->EXP = $EXP;
                    $level->CountAsLevel = 0;
                    $restoreXP = 0;
                } else if (strtolower($request->postVar('val')) == 'lich') {
                    $this->warband->Lich = 1;
                    $level->CountAsLevel = 0;
                    $this->warband->EXP = $EXP;
                    $restoreXP = 0;
                } else {
                    $level->{$request->postVar('val')} = 1;
                }
                if ((int) $request->postVar('id') > 0) {
                    $level->SpellID = (int) $request->postVar('id');
                }
                $level->WarbandID = $this->warband->ID;

                $level->EXP = $restoreXP;
                $lvl = $level->write();
                $this->warband->write();
                $this->warband->logEvent('Level', $lvl, $request->postVar('val'));
                if ((int) $request->postVar('item') > 0) {
                    $item = DataObject::get_by_id('fguseritems', (int) $request->postVar('item'));
                    $item->delete();
                }
            }
        }
    }

    public function addtrick(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $level = new fgusertricks();
                if (strtolower($request->postVar('val')) == 'trick') {
                    $level->TrickID = (int) $request->postVar('id');
                } else {
                    $level->{$request->postVar('val')} = 1;
                }
                $level->WarbandID = $this->warband->ID;
                $lvl = $level->write();
                if ($this->warband->CaptainTricks()->Count() > 2) {
                    $this->warband->CEXP = $this->warband->CEXP - 100;
                }

                $this->warband->write();
                $this->warband->logEvent('Trick', $lvl, $request->postVar('val'));
            }
        }
    }

    public function removetrick(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $trick = DataObject::get_by_id('fgusertricks', (int) $request->postVar('id'));
                if ($trick) {
                    $trick->delete();
                }
            }
        }
    }

    public function usetrick(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $trick = DataObject::get_by_id('fgusertricks', (int) $request->postVar('id'));
                $trick->Used = ($trick->Used == 1 ? 0 : 1);
                $trick->write();
            }
        }
    }

    public function undolevel(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $origXP = $this->warband->EXP;
                $level = DataObject::get_by_id('fguserwizardlevels', (int) $request->postVar('id'));
                if ($level) {
                    $this->warband->EXP = $origXP + $level->EXP;
                    $this->warband->write();
                    $level->delete();
                }
            }
        }
    }

    public function removespell(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $spell = DataObject::get_by_id('fguserspells', (int) $request->postVar('id'));
                if ($spell) {
                    foreach ($this->warband->WizardLevels()->filter('SpellID', array($spell->Spell()->ID)) as $level) {
                        $level->delete();
                    }
                    $spell->delete();
                }
            }
        }
    }

    public function addbeastcraftertrait(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->warband->BeastcrafterTraitID = (int) $request->postVar('id');
                $this->warband->write();
                $this->warband->logEvent('Beastcrafter Trait', (int) $request->postVar('id'), 'Set');
            }
        }
    }

    public function removebeastcrafter(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $levels = $this->warband->WizardLevels()->filter('BeastCrafter', array(1));
                foreach ($levels as $level) {
                    $level->delete();
                    $this->warband->EXP = $this->warband->EXP + $level->EXP;
                }
                $this->warband->BeastcrafterTraitID = 0;
                $this->warband->BeastcrafterLevelID = 0;
                $this->warband->write();
                $this->warband->logEvent('Beastcrafter Trait Removed', (int) $request->postVar('id'), 'Set');
            }
        }
    }

    public function removelich(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $levels = $this->warband->WizardLevels()->filter('Lich', array(1));
                foreach ($levels as $level) {
                    $level->delete();
                }
                $this->warband->Lich = 0;
                $this->warband->write();
                $this->warband->logEvent('Lichdom Removed', (int) $request->postVar('id'), 'Set');
            }
        }
    }

    public function homebase(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $this->warband->HomebaseID = (int) $request->postVar('id');
                $this->warband->write();
                $this->warband->logEvent('Base', (int) $request->postVar('id'), 'Set');
            }
        }
    }

    public function buyresource(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $resource = new fguserresources();
                $resource->WarbandID = $this->warband->ID;
                $resource->ResourceID = (int) $request->postVar('id');
                $resource->write();
                $this->warband->logEvent('Base', (int) $request->postVar('id'), 'Add Resource');
            }
        }
    }

    public function removeresource(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $resource = DataObject::get_by_id('fguserresources', (int) $request->postVar('id'));
                if ($resource) {
                    $resource->delete();
                }
            }
        }
    }

    public function renameunit(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $unit = $this->warband->WarbandUnits()->filter('id', array((int) $request->postVar('id')))->first();

                //$unit = DataObject::get_by_id('fguserunit', (int)$request->postVar('id'));
                if ($unit) {
                    if ($unit->Type == 'Wizard') {
                        $this->warband->WizardName = $request->postVar('name');
                        $this->warband->write();
                    }
                    $unit->Name = $request->postVar('name');
                    $unit->write();
                }
            }
        }
    }

    public function setillusion(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $unit = $this->warband->WarbandUnits()->filter('id', array((int) $request->postVar('id')))->first();
                if ($unit) {
                    $unit->Illusion = $unit->Illusion = ($unit->Illusion == 1 ? 0 : 1);
                    $unit->write();
                }
            }
        }
    }

    public function PurchaseCost($val, $id) {
        $demonic = 0;
        $beastMaster = (int) $val + (int) $this->warband->BeastcrafterLevel()->SoldierCost;
        $baseAdjust = 0;
        // adjust for brewery
        if ($this->warband->Homebase()->ID == 6) {
            $baseAdjust = -5;
        }

        // this is a demon hunter, need to make some cost adjustments
        //Wizard knows one or more of the following spells: Summon Demon, Imp, Possess +25gc
        //Wizard is a Summoner. +25gc
        //Wizard's base is equipped with a summoning circle. +50gc
        if ($id == 76) {
            $demonic = 0;
            $demonicSpells = $this->warband->Spells()->filter('SpellID', array(50, 44, 49))->count();
            if ($demonicSpells > 0) {
                $demonic = $demonic + 25;
            }
            if ($this->warband->SchoolID == 6) {
                $demonic = $demonic + 25;
            }
            foreach ($this->warband->BaseResources() as $resource) {
                if ($resource->Resource()->ID == 15)
                    $demonic = $demonic + 50;
            }
        }
        $cost = $beastMaster + $demonic + $baseAdjust;
        return $cost;
    }

    public function addmark(SS_HTTPRequest $request) {
        //print_r($request);
        //exit();
        if ($request->isAjax()) {
            if ($request->postVar('unit')) {

                //get unit
                $unit = $this->warband->WarbandUnits()->filter('id', array((int) $request->postVar('unit')))->first();
                //print $unit->ID.' '.$unit->Name;
                $markID = (int) $request->postVar('mark');
                $unit->MarkID = $markID;
                $unit->write();
                //exit();
            }
        }
    }

    public function removemark(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($request->postVar('unit')) {
                $unit = $this->warband->WarbandUnits()->filter('id', array((int) $request->postVar('unit')))->first();
                //print $unit->ID.' '.$unit->Name;
                $unit->MarkID = 0;
                $unit->write();
                //exit();
            }
        }
    }

    public function addpact(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $this->warband->DemonicPact = 1;
            $this->warband->write();
            $this->warband->logEvent('Forged Demonic Pact', (int) $request->postVar('id'), 'Set');
            // remove an True name items from the vault
            $items = $this->warband->Items()->filter(array('ItemID' => 134));
            foreach ($items as $item) {
                $item->delete();
                $this->warband->logEvent('Item', 134, 'deleted');
            }
            // add demonic servent grimoire
            $fguseritems = new fguseritems();
            $fguseritems->WarbandID = $this->warband->ID;
            $fguseritems->ItemID = 11;
            $fguseritems->SpellID = 92;
            $fguseritems->write();
        }
    }

    public function removepact(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $this->warband->DemonicPact = 0;
            $this->warband->write();
            foreach ($this->warband->Boons() as $boon) {
                $boon->delete();
            }
            foreach ($this->warband->Sacrifices() as $sacrifice) {
                $sacrifice->delete();
            }
            $this->warband->logEvent('Demonic Pact broken', (int) $request->postVar('id'), 'Set');
        }
    }

    public function addboon(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $boon = new fguserboons();
                $boon->WarbandID = $this->warband->ID;
                $boon->BoonID = (int) $request->postVar('id');
                $boon->write();
                $this->warband->logEvent('Boon', (int) $request->postVar('id'), 'Add Boon');
            }
        }
    }

    public function removeboon(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $boon = DataObject::get_by_id('fguserboons', (int) $request->postVar('id'));
                if ($boon) {
                    $boon->delete();
                }
            }
        }
    }

    public function addsacrifice(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $sacrifice = new fgusersacrifices();
                $sacrifice->WarbandID = $this->warband->ID;
                $sacrifice->SacrificeID = (int) $request->postVar('id');
                $sacrifice->write();
                $this->warband->logEvent('Sacrifice', (int) $request->postVar('id'), 'Add Sacrifice');
            }
        }
    }

    public function removesacrifice(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($this->getRequest()->param('ID')) {
                $sacrifice = DataObject::get_by_id('fgusersacrifices', (int) $request->postVar('id'));
                if ($sacrifice) {
                    $sacrifice->delete();
                }
            }
        }
    }

    /**
      ADD AND REMOVE NOTES FROM THE WARBAND
     * */
    public function addnote(SS_HTTPRequest $request) {
        //print_r($request);
        //exit();
        /*
          if ($request->isAjax()) {
          if ($request->postVar('unit')) {

          //get unit
          $unit = $this->warband->WarbandUnits()->filter('id', array((int) $request->postVar('unit')))->first();
          //print $unit->ID.' '.$unit->Name;
          $markID = (int) $request->postVar('mark');
          $unit->MarkID = $markID;
          $unit->write();
          //exit();
          }
          } */
    }

    public function removenote(SS_HTTPRequest $request) {
        /*
          if ($request->isAjax()) {
          if ($request->postVar('note')) {
          $note = $this->warband->Notes()->filter('id', array((int) $request->postVar('note')))->first();
          $note->delete();
          //exit();
          }
          } */
    }

    /**
      ADD AND REMOVE NOTES FROM THE WARBAND
     * */
    public function addmount(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($request->postVar('unit')) {
                $unit = $this->warband->WarbandUnits()->filter('id', array((int) $request->postVar('unit')))->first();
                //print $unit->ID.' '.$unit->Name;
                $mountID = (int) $request->postVar('id');
                $unit->MountID = $mountID;
                $unit->write();
            }
        }
    }

    public function removemount(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            if ($request->postVar('unit')) {
                $unit = $this->warband->WarbandUnits()->filter('id', array((int) $request->postVar('unit')))->first();
                //print $unit->ID.' '.$unit->Name;
                $unit->MountID = 0;
                $unit->write();
                //exit();
            }
        }
    }

}
