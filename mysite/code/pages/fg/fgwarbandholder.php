<?php

class fgwarbandholder extends GamingApp {

    private static $db = array(
    );
    private static $has_one = array(
    );

}

class fgwarbandholder_controller extends GamingApp_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'addwarband'
    );

    public function init() {
        parent::init();
        Requirements::javascript('mysite/js/fg/warband.js');
        //if($this->accountStatus() == 'PAID') {
            Requirements::css('themes/'.SSViewer::current_theme()."/css/fg-class.css");    
        //}        
    }



    public function NumWarbands() {
        if (Member::currentUser() && Member::currentUser()->ID) {
            return fguserwarband::get()->filter(array('MemberID' => Member::currentUser()->ID))->Count();
        } else {
            return 0;
        }
    }
    public function canCreateWarband() {
        /*
         * Check if this users ID has an associated clubID
         * clubs can add a number of members and removes the warband limit
         */

        /*
         * If not in a club, then check the individual user level
         */
        if (
                $this->accountStatus() == 'FREE' && $this->NumWarbands() >= 1
        ) {
            return false;
        }
        return true;
    }

    public function addwarband() {
        if ($this->canCreateWarband() == true) {
            //$this->request->postVars();
            $warband = new fguserwarband();
            $warband->Funds = 500;
            $warband->EXP = 0;
            $warband->SchoolID = $this->request->postVar('WizardSchool');
            $warband->WizardName = $this->request->postVar('WizardName');
            $warband->MemberID = Member::currentUser()->ID;
            $warbandID = $warband->write();

            foreach ($this->request->postVar('Spell') as $spell) {
                $addSpell = new fguserspells();
                $addSpell->SpellID = $spell;
                $addSpell->WarbandID = $warbandID;
                $addSpell->write();
            }
            $this->redirectBack($this->Link() . '?warbandCreated=1');

            $wizard = new fguserunits();
            $wizard->Type = 'Wizard';
            $wizObj = DataObject::get_one('fgunit', array(
                        'Type' => 'Wizard'
            ));
            $wizard->UnitID = $wizObj->ID;
            $wizard->Name = $this->request->postVar('WizardName');
            $wizard->WarbandID = $warbandID;
            $wizard->write();
        }
        $this->redirectBack($this->Link() . '?maxreached=1');
    }
}
