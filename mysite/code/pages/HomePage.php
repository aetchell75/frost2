<?php

class HomePage extends Page {

    private static $db = array(
    );
    private static $has_one = array(
    );
    private static $has_many = array(
        'OnlineListBuilders' => 'OnlineListBuilders',
        'UpdateLog' => 'UpdateLog'    
    );

}

class HomePage_Controller extends Page_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
        // You can include any CSS or JS required by your project here.
        // See: http://doc.silverstripe.org/framework/en/reference/requirements
        Requirements::javascript('themes/'.SSViewer::current_theme().'/js/owl/owl.carousel.min.js');
        Requirements::css('themes/'.SSViewer::current_theme().'/js/owl/assets/owl.carousel.min.css'); 
        Requirements::css('themes/'.SSViewer::current_theme().'/js/owl/assets/owl.theme.default.css'); 
    }  
    
    public function FgUpdates() {
        $updates = DataObject::get('Article')->filter('Type',array('Frostgrave'))->Sort('LastEdited','DESC')->Limit(1);
        return $updates;
    }

    public function DMHUpdates() {
        $updates = DataObject::get('Article')->filter('Type',array('Dead Mans Hand'))->Sort('LastEdited','DESC')->Limit(1);
        return $updates;
    }
}
