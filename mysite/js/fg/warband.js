var $_Block = {
    message: '<h2>Updating Warband</h2>',
    css: {border: '0px'},
    overlayCSS: {
        backgroundColor: '#fff',
//            opacity:         0.6,
//            cursor:          'wait'
    },
};
document.addEventListener("DOMContentLoaded", function (event) {
    //do work

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({container: 'body'});
        $('[data-toggle="popover"]').popover({html: true});
        $('.unit-block.matchHeight').matchHeight();

//    $(document).on('click', '#DoCreateWarband').click(function(e) {
//        e.preventDefault();
//        e.stopPropagation();
//        $('#CreateWarband').submit();
//    });

        $(document).on('click', '.UnitDismiss', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $conf = confirm('Are you sure you want to dismiss this unit?');
            if ($conf === true) {
                $(this).closest('tr').fadeOut('fast');
                $('#warbandContainer').block($_Block);
                $.post(warbandurl + 'unitdismiss/' + warbandhash,
                        {id: $(this).data('id')},
                        function (res) {
                            _rebindWarband();
                        }
                );
                ga('send', 'event', 'Frostgrave-warBandAction', 'unitDimiss');
            }
        });


        $(document).on('click', '.addMutation', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addmutation/' + warbandhash,
                    {id: $(this).closest('.dropdown').find('ul').data('id'), trait: $(this).data('trait')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addMutation');
        });

        $(document).on('click', '.CaptainDismiss', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $(this).closest('#captain-block').fadeOut('fast');
            $.post(warbandurl + 'unitdismiss/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'captainDismiss');
        });

        $(document).on('click', '.ApprenticeDismiss', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $(this).closest('#apprentice-block').fadeOut('fast');
            $.post(warbandurl + 'unitdismiss/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'apprenticeDismiss');
        });

        /**
         * CAPTAIN
         */
        $(document).on('click', '.CaptainBuy', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $('#addCaptain').modal('hide');
            $.post(warbandurl + 'unitbuy/' + warbandhash,
                    {cost: $(this).data('cost'), id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'captainBuy');
        });

        $(document).on('click', '.ApprenticeBuy', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'unitbuy/' + warbandhash,
                    // fix the static values in this one
                            {cost: $(this).data('cost'), id: $(this).data('id')},
                            function (res) {
                                _rebindWarband();
                            }
                    );
                    ga('send', 'event', 'Frostgrave-warBandAction', 'ApprenticeBuy');
                });

        $(document).on('click', '.UnitBuy', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'unitbuy/' + warbandhash,
                    {cost: $(this).data('cost'), id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'UnitBuy', $(this).data('id'), $(this).data('cost'));
        });

        $(document).on('click', '.AddSpell', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addspell/' + warbandhash,
                    {spell: $(this).data('spell')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addSpell');
        });

        /**
         * ITEM
         */
        $(document).on('click', '.ItemBuy', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'itembuy/' + warbandhash,
                    {cost: $(this).data('cost'), id: $(this).data('id'), spell: $(this).data('spell')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'itemBuy');
        });

        $(document).on('click', '.ItemEquip', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'itemequip/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'itemEquip');
        });

        $(document).on('click', '.assigntobutton', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'itemassign/' + warbandhash,
            {item: $(this).closest('ul').data('item'), id: $(this).data('id')},
                    function(res) {_rebindWarband(); }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'itemAssign');
        });

        $(document).on('click', '.itemStore', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'itemstore/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'itemStore');
        });



        $(document).on('click', '.itemRemove', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $(this).closest('tr').fadeOut('fast');
            $.post(warbandurl + 'itemremove/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'itemRemove');
        });
        $(document).on('click', '.itemSell', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $(this).closest('tr').fadeOut('fast');
            $.post(warbandurl + 'itemsell/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'itemSell');
        });

        $(document).on('click', '.adjustExpModal', function (e) {
            $('#currentExpValue').val($(this).data('exp'));
            $('#addExp').on('shown.bs.modal', function () {
                $('#addExp').find('input:first').focus();
            })
        });
        $(document).on('click', '.adjustExp', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'adjustexp/' + warbandhash,
                    {val: $('#currentExpValue').val()},
                    function (res) {
                        $('#addExp').modal('hide');
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'adjustExp');
        });

        $(document).on('click', '.adjustCexpModal', function (e) {
            $('#currentCexpValue').val($(this).data('cexp'));
            $('#addCexp').on('shown.bs.modal', function () {
                $('#addCexp').find('input:first').focus();
            })
        });
        $(document).on('click', '.adjustCexp', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'adjustcexp/' + warbandhash,
                    {val: $('#currentCexpValue').val()},
                    function (res) {
                        $('#addCexp').modal('hide');
                        _rebindWarband();

                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'adjustCaptainExp');
        });

        $(document).on('click', '.adjustFundsModal', function (e) {
            $('#currentFundsValue').val($(this).data('funds'));
            $('#addFunds').on('shown.bs.modal', function () {
                $('#addFunds').find('input:first').focus();
            })
        });
        $(document).on('click', '.adjustFunds', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'adjustfunds/' + warbandhash,
                    {val: $('#currentFundsValue').val()},
                    function (res) {
                        $('#addFunds').modal('hide');
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'adjustFunds');
        });


        $(document).on('click', '.setInjured', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'setinjured/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'toggleInjured');
        });

        $(document).on('click', '.addInjury', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addinjury/' + warbandhash,
                    {id: $(this).data('id'), unit: $(this).closest('.injury-group').find('.addInjuryActivator').data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addInjury');
        });
        $(document).on('click', '.removeInjury', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removeinjury/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeInjury');
        });

        $(document).on('click', '.treatInjury', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'treatinjury/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'treatInjury');
        });
        $(document).on('click', '.addLevel', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addlevel/' + warbandhash,
                    {id: $(this).data('id'), val: $(this).data('val'), item: $(this).data('item'), level: $(this).data('level')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addLevel');
        });

        $(document).on('click', '.addbeastcraftertrait', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addbeastcraftertrait/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        location.reload();
                        //_rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addBeastcrafterTrait');
        });

        $(document).on('click', '.removeBeastcrafter', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removebeastcrafter/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        location.reload();
                        //_rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeBeastcrafterTrait');
        });
        $(document).on('click', '.removeLich', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removelich/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        location.reload();
                        //_rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeLich');
        });

        $(document).on('click', '.undoLevel', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'undolevel/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'undoLevel');
        });

        $(document).on('click', '.addTrick', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addtrick/' + warbandhash,
                    {id: $(this).data('id'), val: $(this).data('val'), item: $(this).data('item')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addTrick');
        });

        $(document).on('click', '.removeTrick', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $(this).closest('tr').fadeOut('fast');
            $.post(warbandurl + 'removetrick/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeTrick');
        });

        $(document).on('click', '.useTrick', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'usetrick/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'useTrick');
        });

        $(document).on('click', '.removeSpell', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removespell/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeSpell');
        });

        $(document).on('click', '.setHomebase', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'homebase/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'setHomebase');
        });
        $(document).on('click', '.buyResource', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'buyresource/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'buyResource');
        });

        $(document).on('click', '.removeResource', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removeresource/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeResource');
        });
        $(document).on('click', '.setIllusion', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'setillusion/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'setIllusion');
        });


        $(document).on('click', '.removeMark', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removemark/' + warbandhash,
                    {unit: $(this).closest('.MarkList').data('unit')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeDemonicPact');
        });
        $(document).on('click', '.addMark', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addmark/' + warbandhash,
                    {unit: $(this).closest('.MarkList').data('unit'), mark: $(this).data('mark')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addMark');
        });

        $(document).on('click', '.addPact', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addpact/' + warbandhash,
                    {},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addDemonicPact');
        });
        $(document).on('click', '.removePact', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removepact/' + warbandhash,
                    {},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeDemonicPact');
        });

        $(document).on('click', '.addBoon', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addboon/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addBoon');
        });
        $(document).on('click', '.removeBoon', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removeboon/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeBoon');
        });

        $(document).on('click', '.addSacrifice', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addsacrifice/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addSacrifice');
        });
        $(document).on('click', '.removeSacrifice', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removesacrifice/' + warbandhash,
                    {id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeSacrifice');
        });

        $(document).on('click', '.addMount', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'addmount/' + warbandhash,
                    {unit: $(this).closest('div').data('id'), id: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'addMount');
        });

        $(document).on('click', '.removeMount', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('#warbandContainer').block($_Block);
            $.post(warbandurl + 'removemount/' + warbandhash,
                    {unit: $(this).data('id')},
                    function (res) {
                        _rebindWarband();
                    }
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'removeMount');
        });


        $(document).on('click', '.renameUnit', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $name = $(this).closest('.renameUnitForm').find('.newUnitName').val();
            $(this).closest('.dropdown-menu').dropdown('toggle');
            $(this).closest('td').find('.unitsCurrentName').text($name);
            $(this).closest('.renameUnitForm').find('.newUnitName').val($name);
            $.post(warbandurl + 'renameunit/' + warbandhash,
                    {name: $name, id: $(this).closest('.renameUnitForm').data('id')},
                    function (res) {}
            );
            ga('send', 'event', 'Frostgrave-warBandAction', 'renameUnit', $name);
        });

        /*
         * Table sorters
         */
        var addsoldiersList = new List('addsoldiers', {valueNames: ['Sname', 'Sgear']});
        var addsummonedList = new List('addsummoned', {valueNames: ['Sname', 'Stype', 'Sgear']});
        var addweaponsList = new List('items-weapons', {valueNames: ['Sname', 'Sdesc', 'Suse']});
        var addarmourtemsList = new List('items-armour', {valueNames: ['Sname', 'Sdesc', 'Suse']});
        var additemsitemsList = new List('items-items', {valueNames: ['Sname', 'Sdesc', 'Suse']});
        var addgrimouresitemsList = new List('items-grimoire', {valueNames: ['Sname', 'Sschool', 'Suse']});
        var addscrollsitemsList = new List('items-scrolls', {valueNames: ['Sname', 'Sdesc', 'Suse']});
        var addpotionsitemsList = new List('items-potions', {valueNames: ['Sname', 'Sdesc']});
        var addcaptainitemsList = new List('items-captain', {valueNames: ['Sname', 'Sdesc', 'Suse']});

    });

    function _rebindWarband() {
        $('[data-toggle="tooltip"]').tooltip('hide');
        //$('#warbandContainer').block($_Block);
        $.get(warbandurl + 'reloadwarband/' + warbandhash, {}, function (res) {
            $('#warbandContainer').html(res);
            $('#warbandContainer').unblock();
        });
        $('[data-toggle="tooltip"]').tooltip({container: 'body'});
        $('[data-toggle="popover"]').popover();
        ga('send', 'pageview', warbandurl + 'reload');
    }

});