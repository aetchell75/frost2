var $_Block = { 
        message: '<h2>Updating Warband</h2>', 
        css: { border: '0px' } ,
        overlayCSS:  { 
            backgroundColor: '#fff', 
//            opacity:         0.6, 
//            cursor:          'wait' 
        },         
    };
document.addEventListener("DOMContentLoaded", function(event) { 
  //do work

$(document).ready(function() {		
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});  
    $('[data-toggle="popover"]').popover({html:true});    
    $('.matchHeight').matchHeight({byRow:true});

    $(document).on('click', '.UnitDismiss', function(e){
        e.preventDefault();
        e.stopPropagation();
        $conf = confirm('Are you sure you want to dismiss this unit?');
        if($conf === true) {
            $(this).closest('tr').fadeOut('fast');    
            $('#posseContainer').block($_Block);   
            $.post( posseurl + 'unitdismiss/'+possehash,
                {id: $(this).data('id')},
                function(res) {_rebindPosse();}
            );            
        }
    });  
    
    $(document).on('click', '.UnitBuy', function(e){
        if($('#PosseData').data('freerep') >= $(this).data('cost')) {
            e.preventDefault();
            e.stopPropagation();$('#posseContainer').block($_Block);   
            $.post( posseurl + 'unitbuy/'+possehash,
                {cost: $(this).data('cost'),id: $(this).data('id')},
                function(res) {_rebindPosse();}
            );            
        }
    });    

    $(document).on('click','.adjustRepModal', function(e) {
        $('#currentRepValue').val($(this).data('rep'));
        $('#addRep').on('shown.bs.modal', function () {
          $('#addRep').find('input:first').focus();
        })        
    });
    
    $(document).on('click','.adjustRep', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#posseContainer').block($_Block);   
        $.post( posseurl + 'adjustrep/'+possehash,
            {val: $('#currentRepValue').val()},
            function(res) {
                $('#addRep').modal('hide'); 
                _rebindPosse();
                
            }
        );        
    });   
    
    $(document).on('change', '.gangmemberweapon', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $.post( posseurl + 'setweapon/'+possehash,
            {
                id: $(this).data('id'),
                weapon: $(this).val()
            },
            function(res) {
                console.log(res);
            }
        );           
    });

    $(document).on('click','.renameUnit', function(e) {
        e.preventDefault();
        e.stopPropagation(); 
        $name = $(this).closest('.renameUnitForm').find('.newUnitName').val();
        $(this).closest('.dropdown-menu').dropdown('toggle');
        $(this).closest('td').find('.unitsCurrentName').text($name);
        $(this).closest('.renameUnitForm').find('.newUnitName').val($name);        
        $.post( posseurl + 'renameunit/'+possehash,
            {name:$name,id: $(this).closest('.renameUnitForm').data('id')},
            function(res) {}
        );        
    }); 
	
});

function _rebindPosse() {
    $('[data-toggle="tooltip"]').tooltip('hide');    
    //$('#posseContainer').block($_Block);     
    $.get(posseurl+'reloadposse/'+possehash,{},function(res){
        $('#posseContainer').html(res);
        $('#posseContainer').unblock(); 
    });
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});  
    $('[data-toggle="popover"]').popover();    
}

});