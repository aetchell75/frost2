var paperTheme = [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "hue": "#0066ff"
            },
            {
                "saturation": 74
            },
            {
                "lightness": 100
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "weight": 0.6
            },
            {
                "saturation": -85
            },
            {
                "lightness": 61
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#5f94ff"
            },
            {
                "lightness": 26
            },
            {
                "gamma": 5.86
            }
        ]
    }
];



var mapWidth = $('#ClubMap').width();
var overlayWidth = 300;
var mapmarkers = [];
function initMap() {
    var options = {
        zoom: 3,
        center: {lat: 1.3521, lng: 103.8198},
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_RIGHT,
            mapTypeIds: ['Satallite', 'Terrain',]
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        //styles: paperTheme,
        scaleControl: true,
        streetViewControl: false,
        fullscreenControl: false,
    };              
    var map = new google.maps.Map(document.getElementById('ClubMap'), options); 
    map.setOptions({mapTypeId: 'terrain'});
    
    $.get('/club-finder/clublist/', function (dataRaw) {
        //console.log(data);
        var data = JSON.parse(dataRaw);
        for (i = 0; i < data.length; i++) {
            var obj = data[i];
            //console.log(obj);
            var marker = new google.maps.Marker({
                position: {lat: parseFloat(obj.Lat), lng: parseFloat(obj.Lng)},
                map: map,
                title: obj.Name,
                icon: {
                    url: '/themes/tabletop-space/img/club-pin.png',
                    scaledSize: new google.maps.Size(28, 41)
                },
                id: obj.ID
            });

            google.maps.event.addListener(marker, 'click', (function (marker) {
                return function () {
                    map.panTo(this.getPosition());
                    map.setZoom(10);
                    marker.setVisible(true);
                }
            })(marker));
            google.maps.event.addListener(marker, 'click', (function (marker) {
                return function () {
                    map.panTo(this.getPosition());
                    map.setZoom(7);
                    $('.locationItem').hide();
                    $('#location-' + marker.id).show();
                }
            })(marker));
            mapmarkers.push(marker);
        }
        var clubs = data;
    });
}
$(document).ready(function () {
    $(window).resize(function () {
        h = $(window.top).height() - $('#header-block').outerHeight()
        $('#ClubMap').height(h);
    });
    $(window).trigger('resize');
});
