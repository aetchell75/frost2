 
<div class="container-fluid no-pad">
    <div id="LocationDetailsContainer">
        <% if $Clubs %>
            <% loop $Clubs %>
                <div class="locationItem" id="location-{$ID}">
                    
                    <div class="locationItemInner">
                        <a href="{$Top.Link}club/{$ID}"><h3>{$Title}</h3></a>
                        <div class="locationItemLinks">
                            <% if $Website %><a href="$Website" target="_blank" title="Club website"><i class="fa fa-globe" aria-hidden="true"></i></a> <% end_if %>
                            <% if $Facebook %><a href="$Facebook" target="_blank"><i class="fa fa-facebook-official fb-blue" aria-hidden="true"></i></a> <% end_if %>
                            <% if $Twitter %><a href="$Twitter" target="_blank"><i class="fa fa-twitter twitter-blue" aria-hidden="true"></i></a> <% end_if %>
                            <% if $Google %><a href="$Google" target="_blank"><i class="fa fa-google-plus-official google-red" aria-hidden="true"></i></a> <% end_if %>
                        </div>
                        <div class="locationItemAddress">
                            <% if $Address %><div><span class="locationItemLabel">Address</span>$Address</div><% end_if %>
                            <%--
                            <% if $ContactName %><div><span class="locationItemLabel">Contact</span>$ContactName</div><% end_if %>
                            <% if $ContactEmail %><div><span class="locationItemLabel">Email</span>$ContactEmail</div><% end_if %>
                            <% if $ContactNumber %><div><span class="locationItemLabel">Call</span>$ContactNumber</div><% end_if %>    
                            --%>
                        </div>

                        
                    </div>
                    <a href="{$Top.Link}club/{$ID}" class="learn-more">View club details</a>

                </div>
            <% end_loop %>
        <% end_if %>
    </div>     
    <div class="" id="ClubMap"></div>
</div><!-- 
<div class="container">
    <div class="row">
        <div class="col-xs-12">
                <h1>$Title</h1>
                <div class="content">$Content</div>
        </div>
    </div>
</div> -->
<script>
    <% if $ClubsJson %>
    var locations = {$ClubsJson};
    <% else %>
    var locations = [];    
    <% end_if %>

</script>
$Form