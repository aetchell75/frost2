<script>
    var warbandhash = '{$Warband.hash}', warbandurl = '{$Link}';
</script>
<div class="container hidden-print">
    <div class="row">
        <div class="col-xs-12">
            <ul class="nav nav-tabs pull-right" role="tablist">
                <li><a href="{$Parent.Link}"><i class="ra ra-double-team ra-lg"></i> Warband List</a></li>
                <li><a href="{$Link}view/{$Warband.hash}"><i class="ra ra-double-team ra-lg"></i> Warband</a></li>
                <li><a href="{$Link}print/{$Warband.hash}" target="_blank"><i class="ra ra-book ra-lg"></i> Print</a></li>
                <li><a href="{$Link}logs/{$Warband.hash}"><i class="ra ra-quill-ink ra-lg"></i> Log</a></li>
                <li><a href="{$Link}qrs/{$Warband.hash}"><i class="ra ra-wolf-head ra-lg"></i> QR Sheet</a></li>
                <li><a href="{$Link}help/{$Warband.hash}"><i class="ra ra-double-team ra-lg"></i> Help</a></li>
            </ul> 
        </div>
    </div>
    <div id="warbandContainer">
            <h1 class="sr-only">$Warband.WizardName, the $Warband.School.Name Logs</h1>
            <span class="h3">$Warband.WizardName, the $Warband.School.Name Logs</span>        
        <% if $Warband.levelLog.Count > 0 %>
        <div class="col-sm-6">
            <h6>Level Log</h6>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Event</th>
                        <th>Cost</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <% loop $Warband.levelLog %>
                    <tr>
                        <td>$Created.format('d M Y H:i')</td>
                        <td>
                            <% if $Level.NewSpell >= 1 %>
                            <strong>Learned</strong> $Level.Spell.Name
                            <% else_if $Level.NewSpell < 1 && Level.Spell.Name %>
                            <strong>Improved</strong>  $Level.Spell.Name
                            <% else %>
                            <strong>{$Note}</strong>
                            <% end_if %>
                        </td>
                        <td>{$EXP}</td>
                        <td><a href="" data-id="{$LevelID}" class="btn btn-xs btn-danger undoLevel">Undo Level</a></td>
                    </tr>
                    <% end_loop %>
                </tbody>
            </table>
        </div>




        <% end_if %>
    </div>
</div>

