<div class="modal fade" tabindex="-1" role="dialog" id="addItem">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add or Purchase Items</h4>
            </div>
            <div class="modal-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#items-weapons" aria-controls="items-weapons" role="tab" data-toggle="tab"><i class="ra ra-sword ra-lg"></i> Weapons</a></li>
                        <li role="presentation"><a href="#items-armour" aria-controls="items-armour" role="tab" data-toggle="tab"><i class="ra ra-knight-helmet ra-lg"></i> Armour</a></li>
                        <li role="presentation"><a href="#items-items" aria-controls="items-items" role="tab" data-toggle="tab"><i class="ra ra-slash-ring ra-lg"></i> Items</a></li>

                        <li role="presentation"><a href="#items-grimoire" aria-controls="items-grimoire" role="tab" data-toggle="tab"><i class="ra ra-book ra-lg"></i> Grimoires</a></li>

                        <li role="presentation"><a href="#items-scrolls" aria-controls="items-scrolls" role="tab" data-toggle="tab"><i class="ra ra-scroll-unfurled ra-lg"></i> Scrolls</a></li>

                        <li role="presentation"><a href="#items-potions" aria-controls="items-potions" role="tab" data-toggle="tab"><i class="ra ra-round-bottom-flask ra-lg"></i> Potions</a></li>

                        <% if $Warband.hasCaptain %>

                        <li role="presentation"><a href="#items-captain" aria-controls="items-captain" role="tab" data-toggle="tab"><i class="ra ra-castle-flag ra-lg"></i> Captain</a></li>

                        <% end_if %>





                    </ul>



                    <!-- Tab panes -->

                    <div class="tab-content">

                        <% if $Warband.hasCaptain %>

                        <div role="tabpanel" class="tab-pane" id="items-captain">

                            <input class="search" placeholder="Search" />

                            <button class="sort" data-sort="Sname">Sort by Name</button>

                            <button class="sort" data-sort="Suse">Sort by Use</button>

                            <table class="table table-striped">

                                <thead>

                                    <tr>

                                        <th class="item">Item</th>

                                        <th class="desc">Desc</th>

                                        <th class="uses">Uses</th>

                                        <th class="cost">Cost</th>

                                        <th class="options"></th>

                                    </tr>

                                </thead>

                                <tbody class="list">

                                    <% loop $getItems(captain) %>

                                    <tr>

                                        <td class="Sname"><strong>{$Name}</strong></td>

                                        <td class="Sdesc">{$Desc}</td>

                                        <td class="Suse">{$Use}</td>

                                        <td><% if $Cost > 0 %>{$Cost}gc<% end_if %></td>

                                        <td class="addbuttons text-right">

                                            <div class="btn-group">

                                                <% if $Cost > 0 %><a href="" data-id="{$ID}" data-cost="{$Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a><% end_if %>

                                                <a href="" data-id="{$ID}" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                </tbody>

                            </table>

                        </div>

                        <% end_if %>





                        <div role="tabpanel" class="tab-pane active" id="items-weapons">

                            <input class="search" placeholder="Search" />

                            <button class="sort" data-sort="Sname">Sort by Name</button>

                            <button class="sort" data-sort="Suse">Sort by Use</button>

                            <table class="table table-striped">

                                <thead>

                                    <tr>

                                        <th class="item">Item</th>

                                        <th class="desc">Desc</th>

                                        <th class="uses">Uses</th>

                                        <th class="cost">Cost</th>

                                        <th class="options"></th>

                                    </tr>

                                </thead>

                                <tbody class="list">

                                    <% loop $getItems(weapon) %>

                                    <tr>

                                        <td class="Sname"><strong>{$Name}</strong></td>

                                        <td class="Sdesc">{$Desc}</td>

                                        <td class="Suse">{$Use}</td>

                                        <td><% if $Cost > 0 %>{$Cost}gc<% end_if %></td>

                                        <td class="addbuttons text-right">

                                            <div class="btn-group">

                                                <% if $Cost > 0 %><a href="" data-id="{$ID}" data-cost="{$Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a><% end_if %>

                                                <a href="" data-id="{$ID}" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                </tbody>

                            </table>

                        </div>

                        <div role="tabpanel" class="tab-pane" id="items-armour">

                            <input class="search" placeholder="Search" />

                            <button class="sort" data-sort="Sname">Sort by Name</button>

                            <button class="sort" data-sort="Suse">Sort by Use</button>

                            <table class="table table-striped">

                                <thead>

                                    <tr>

                                        <th class="item">Item</th>

                                        <th class="desc">Desc</th>

                                        <th class="uses">Uses</th>

                                        <th class="cost">Cost</th>

                                        <th class="options"></th>

                                    </tr>

                                </thead>

                                <tbody class="list">

                                    <% loop $getItems(armour) %>

                                    <tr>

                                        <td class="Sname"><strong>{$Name}</strong></td>

                                        <td class="Sdesc">{$Desc}</td>

                                        <td class="Suse">{$Use}</td>

                                        <td><% if $Cost > 0 %>{$Cost}gc<% end_if %></td>

                                        <td class="addbuttons">

                                            <div class="btn-group text-right">

                                                <% if $Cost > 0 %><a href="" data-id="{$ID}" data-cost="{$Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a><% end_if %>

                                                <a href="" data-id="{$ID}" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                </tbody>

                            </table>

                        </div>

                        <div role="tabpanel" class="tab-pane" id="items-items">

                            <input class="search" placeholder="Search" />

                            <button class="sort" data-sort="Sname">Sort by Name</button>

                            <button class="sort" data-sort="Suse">Sort by Use</button>

                            <table class="table table-striped">

                                <thead>

                                    <tr>

                                        <th class="item">Item</th>

                                        <th class="desc">Desc</th>

                                        <th class="uses">Uses</th>

                                        <th class="cost">Cost</th>

                                        <th class="options"></th>

                                    </tr>

                                </thead>

                                <tbody class="list">

                                    <% loop $getItems(item) %>

                                    <tr>

                                        <td class="Sname"><strong>{$Name}</strong></td>

                                        <td class="Sdesc">{$Desc}</td>

                                        <td class="Suse">{$Use}</td>

                                        <td><% if $Cost > 0 %>{$Cost}gc<% end_if %></td>

                                        <td class="addbuttons">

                                            <div class="btn-group text-right">

                                                <% if $Cost > 0 %><a href="" data-id="{$ID}" data-cost="{$Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a><% end_if %>

                                                <a href="" data-id="{$ID}" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                </tbody>

                            </table>

                        </div>

                        <div role="tabpanel" class="tab-pane" id="items-grimoire">

                            <input class="search" placeholder="Search" />

                            <button class="sort" data-sort="Sname">Sort by Name</button>

                            <button class="sort" data-sort="Suse">Sort by Use</button>

                            <table class="table table-striped">

                                <thead>

                                    <tr>

                                        <th class="item">Item</th>

                                        <th class="desc">School</th>

                                        <th class="uses">Uses</th>

                                        <th class="cost">Cost</th>

                                        <th class="options"></th>

                                    </tr>

                                </thead>

                                <tbody class="list">

                                    <% loop $getGrimoires %>

                                    <tr>

                                        <td class="Sname"><strong>{$Grimoire.Name}</strong> of {$Spell.Name}</td>

                                        <td class="Sschool">{$Spell.School.Name}</td>

                                        <td class="Suse">{$Grimoire.Use}</td>

                                        <td><% if $Spell.StartingSpell == 'Y' %>{$Grimoire.Cost}gc<% end_if %></td>

                                        <td class="addbuttons text-right">

                                            <div class="btn-group">

                                                <% if $Spell.StartingSpell == 'Y' %>

                                                <a href="" data-spell="{$Spell.ID}" data-id="{$Grimoire.ID}" data-cost="{$Grimoire.Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a>

                                                <% end_if %>

                                                <a href="" data-spell="{$Spell.ID}" data-id="{$Grimoire.ID}" data-cost="0" data-name="$Grimoire.Name" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                                <a href="" data-spell="{$Spell.ID}" class="AddSpell btn btn-sm btn-info" data-toggle="tooltip" data-placement="left" title="Add Spell directly to warband">Add Spell</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                </tbody>

                            </table>

                        </div>

                        <div role="tabpanel" class="tab-pane" id="items-scrolls">

                            <input class="search" placeholder="Search" />

                            <button class="sort" data-sort="Sname">Sort by Name</button>

                            <table class="table table-striped">

                                <thead>

                                    <tr>

                                        <th class="item">Item</th>

                                        <th class="desc">Desc</th>

                                        <th class="uses">Uses</th>

                                        <th class="cost">Cost</th>

                                        <th class="options"></th>

                                    </tr>

                                </thead>

                                <tbody class="list">

                                    <% loop $getItems(ivory-scroll) %>

                                    <tr>

                                        <td class="Sname"><strong>{$Name}</strong></td>

                                        <td class="Sdesc">{$Desc}</td>

                                        <td>{$Use}</td>

                                        <td></td>

                                        <td class="addbuttons text-right">

                                            <div class="btn-group">

                                                <a href="" data-id="{$ID}" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                    <% loop $getScrolls %>

                                    <tr>

                                        <td class="Sname"><strong>{$Grimoire.Name}</strong> of {$Spell.Name}</td>

                                        <td class="Sschool">{$Spell.School.Name}</td>

                                        <td>{$Grimoire.Use}</td>

                                        <td>{$Grimoire.Cost}gc</td>

                                        <td class="addbuttons text-right">

                                            <div class="btn-group">

                                                <a href="" data-spell="{$Spell.ID}" data-id="{$Grimoire.ID}" data-cost="{$Grimoire.Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a>

                                                <a href="" data-spell="{$Spell.ID}" data-id="{$Grimoire.ID}" data-cost="0" data-name="$Grimoire.Name" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>



                                    <% loop $getDistinctScrolls %>

                                    <tr>

                                        <td class="Sname"><strong>{$Grimoire.Name}</strong></td>

                                        <td class="Sschool">{$Spell.School.Name}</td>

                                        <td>{$Grimoire.Use}</td>

                                        <td>{$Grimoire.Cost}gc</td>

                                        <td class="addbuttons text-right">

                                            <div class="btn-group">

                                                <a href="" data-spell="{$Spell.ID}" data-id="{$Grimoire.ID}" data-cost="{$Grimoire.Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a>

                                                <a href="" data-spell="{$Spell.ID}" data-id="{$Grimoire.ID}" data-cost="0" data-name="$Grimoire.Name" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                </tbody>

                            </table>

                        </div>

                        <div role="tabpanel" class="tab-pane" id="items-potions">

                            <input class="search" placeholder="Search" />

                            <button class="sort" data-sort="Sname">Sort by Name</button>

                            <button class="sort" data-sort="Suse">Sort by Use</button>

                            <table class="table table-striped">

                                <thead>

                                    <tr>

                                        <th class="item">Item</th>

                                        <th class="desc">Desc</th>

                                        <th class="uses">Uses</th>

                                        <th class="cost">Cost</th>

                                        <th class="options"></th>

                                    </tr>

                                </thead>

                                <tbody class="list">

                                    <% loop $getItems(potion) %>

                                    <tr>

                                        <td class="Sname"><strong>{$Name}</strong></td>

                                        <td class="Sdesc">{$Desc}</td>

                                        <td>{$Use}</td>

                                        <td><% if $Cost > 0 %>{$Cost}gc<% end_if %></td>

                                        <td class="addbuttons text-right">

                                            <div class="btn-group">

                                                <% if $Cost > 0 %><a href="" data-id="{$ID}" data-cost="{$Cost}" class="ItemBuy btn btn-sm btn-success">Buy</a><% end_if %>

                                                <a href="" data-id="{$ID}" data-cost="0" class="ItemBuy btn btn-sm btn-warning">Add</a>

                                            </div>

                                        </td>

                                    </tr>

                                    <% end_loop %>

                                </tbody>

                            </table>

                        </div>

                    </div>



                </div>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->