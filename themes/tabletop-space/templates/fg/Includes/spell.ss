<div class="panel-group" id="spell-block-{$ID}-{$Spell.ID}" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading spell-panel-heading {$cssClass($Spell.School.Name)} bgbl" role="tab" id="spell-block-heading-{$ID}-{$Spell.ID}">
            <div class="panel-title">
                <div class="row">
                    <div class="col-xs-2 text-center"><strong class="h3">$wizardDiff</strong><br><span class="small">Diff</span></div>
                    <div class="col-xs-8">
                        <a role="button" data-toggle="collapse" data-parent="#spell-block-{$ID}" href="#spell-block-acc-{$ID}-{$Spell.ID}" aria-expanded="true" aria-controls="spell-block-acc-{$ID}-{$Spell.ID}"><span class="h5"><strong>$Spell.Name.LowerCase</strong></span></a>
                        <br><em>$Spell.School.Name</em>
                        <br><em>$Spell.Type</em>
                    </div>

                    <div class="col-xs-2"><a href="" data-id="{$ID}" class="pull-right removeSpell btn btn-xs btn-danger hidden-print" data-toggle="tooltip" data-placement="top" title="Remove Spell"><i class="ra ra-cycle"></i></a></div>
                </div>
            </div>




        </div>
        <div id="spell-block-acc-{$ID}-{$Spell.ID}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="spell-block-heading-{$ID}-{$Spell.ID}">
            <div class="panel-body spell-desc">
                <strong><em>$Spell.School.Name</em> - Base Difficulty: <span data-toggle="tooltip" data-placement="top" title="Base Difficulty">$Spell.Diff</span></strong>
                <div>$Spell.Desc</div>
            </div>
        </div>
    </div>
</div>