<div class="{$cssClass($Warband.School.Name)}">
    <div class="text-right unit-options">


        <div  class="btn-group">
            <% if $Unit.Type == 'Wizard' %>

            <div class="btn-group">

                <button type="button" class="dropdown-toggle addLevelActivator btn btn-sm btn-success hidden-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ra ra-player-lift"></i> Add Level
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-left levelList" data-unit="{$ID}">
                    <% loop $Warband.levelOptions %>
                    <% if $Type == 'Spell' %>
                    <li class="col"><a href="" data-val="Cast" data-id="{$SpellID}" class="addLevel"><i class="ra ra-fairy-wand"></i> {$Name} -1</a></li>
                    <% else_if $Type == 'Beastcrafter' %>
                    <li class="dropdown-header">Become a {$Name}:</li>
                    <li role="separator" class="divider"></li>
                    <li><a href="" data-val="{$Name}" data-level="{$ID}" data-id="{$SpellID}" class="addLevel"><i class="ra ra-pawprint"></i> {$Name}</a></li>
                    <% else_if $Type == 'Lich' %>
                    <% if $Top.Warband.Lich != 1 %>
                    <li class="dropdown-header">Lichdom:</li>
                    <li role="separator" class="divider"></li>
                    <li><a href="" data-val="{$Type}" data-level="{$ID}" class="addLevel"><i class="ra ra-death-skull"></i> {$Name}</a></li>
                    <% end_if %>
                    <% else_if $Type == 'LichFail' %>
                    <% if $Top.Warband.Lich != 1 %>

                    <li><a href="" data-val="{$Type}" data-level="{$ID}" class="addLevel"><i class="ra ra-death-skull"></i> {$Name}</a></li>
                    <% end_if %>
                    <% else %>
                    <li class="col"><a href="" data-val="{$Type}" data-id="{$ID}" class="addLevel"><i class="ra ra-muscle-up"></i> {$Name} +1</a></li>
                    <% end_if %>
                    <% end_loop %>
                    <% if $Warband.learnableGrimoires.Count > 0 %>
                    <li class="dropdown-header">Learn: </li>
                    <li role="separator" class="divider"></li>

                    <% loop $Warband.learnableGrimoires %>
                    <li class="col"><a href="" data-item="{$ItemID}" data-val="Grimoire" data-id="{$SpellID}" class="addLevel"><i class="ra ra-book"></i> {$Name}</a></li>
                    <% end_loop %>
                    <% end_if %>
                </ul>
            </div>
            <% if $Warband.DemonicPact == 1 %>
            <div class="btn-group">
                <button class="dropdown-toggle btn btn-sm btn-danger hidden-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ra ra-wyvern"></i> Demonic Pact <span class="caret"></span></button>
                <ul class="dropdown-menu demonicList" data-unit="{$ID}">
                    <li class="dropdown-header">Boons</li>
                    <% loop $BoonList %>
                    <li class="col"><a href="" data-id="{$ID}" class="addBoon">$Name</a></li>
                    <% end_loop %>

                    <li class="dropdown-header">Sacrifices</li>
                    <li role="separator" class="divider"></li>
                    <% loop $SacrificeList %>
                    <li class="col"><a href="" data-id="{$ID}" class="addSacrifice">$Name</a></li>
                    <% end_loop %>
                </ul>
            </div>
            <button type="button" class="dropdown-toggle removePact btn btn-sm btn-danger hidden-print">
                <i class="ra ra-wyvern"></i> Break Demonic Pact
            </button>
            <% else_if $Warband.canForgePact %>
            <button type="button" class="dropdown-toggle addPact btn btn-sm btn-danger hidden-print">
                <i class="ra ra-wyvern"></i> Forge Demonic Pact
            </button>

            <% end_if %>
            <a href="" data-exp="{$Warband.EXP}" class="adjustExpModal btn btn-sm btn-info hidden-print" data-toggle="modal" data-target="#addExp">Exp: {$Warband.EXP}</a>
            <% end_if %>

            <% if $Unit.Type == 'Captain' %>
            <% if $Warband.CEXP > 100 || $Warband.getCLevels < 2  %>
            <div class="btn-group">
                <button type="button" class="dropdown-toggle addTrickActivator btn btn-sm btn-success hidden-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ra ra-player-lift"></i> Add Level
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-left trickList" data-unit={$ID}>
                    <% loop $Warband.trickOptions %>
                    <% if $Type == 'Trick' %>
                    <li class="col"><a href="" data-val="Trick" data-id="{$TrickID}" class="addTrick"><i class="ra ra-decapitation"></i> {$Name}</a></li>
                    <% else %>
                    <li class="col"><a href="" data-val="{$Type}" data-id="{$ID}" class="addTrick"><i class="ra ra-muscle-up"></i> {$Name} +1</a></li>
                    <% end_if %>
                    <% end_loop %>
                </ul>
            </div>
            <% end_if %>
            <a href="" data-cexp="{$Warband.CEXP}" class="adjustCexpModal btn btn-sm btn-warning hidden-print"  data-toggle="modal" data-target="#addCexp">Exp: {$Warband.CEXP}</a>
            <% end_if %>





            <div class="btn-group injury-group">
                <button type="button" data-id="{$Unit.ID}" class="dropdown-toggle addInjuryActivator btn btn-sm btn-danger hidden-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ra ra-cut-palm" data-toggle="tooltip" data-placement="top" title="{$Warband.BeastcrafterLevel.Name}"></i> Add Injury
                    <span class="caret"></span>
                </button>
                <% include addinjury %>
            </div>

            <% if $Unit.Type == 'Wizard' %>
            <% if $Warband.BeastcrafterLevelID > 0 %>
            <div class="btn-group">
                <button type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ra ra-pawprint" data-toggle="tooltip" data-placement="top" title="{$Warband.BeastcrafterLevel.Name}"></i>
                    <span class="caret"></span>
                    <span class="sr-only">Remove Beastcrafter Status</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="" data-id="{$ID}" class="removeBeastcrafter">
                            <i class="ra ra-cycle"></i> Remove Beastcrafter Status
                        </a>
                    </li>
                </ul>
            </div>


            <% end_if %>
            <% if $Warband.Lich == 1 %>
            <div class="btn-group">
                <button type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ra ra-death-skull" data-toggle="tooltip" data-placement="top" title="Lichdom"></i>
                    <span class="caret"></span>
                    <span class="sr-only">Remove Lich Status</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="" data-id="{$ID}" class="removeLich">
                            <i class="ra ra-cycle"></i> Remove Lich Status
                        </a>
                    </li>
                </ul>
            </div>


            <% end_if %>
            <% end_if %>

            <% if $Unit.Type == 'Captain' %>
            <div class="btn-group">
                <button type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ra ra-cycle" data-toggle="tooltip" data-placement="top" title="Remove from Warband"></i>
                    <span class="caret"></span>
                    <span class="sr-only">Dismiss</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="" data-id="{$Warband.Captain.ID}" class="CaptainDismiss" data-toggle="tooltip" data-placement="top" title="Dismiss the Captain">
                            <i class="ra ra-cycle"></i> Dismiss the Captain
                        </a>
                    </li>
                </ul>
            </div>
            <% end_if %>

            <% if $Unit.Type == 'Apprentice' %>
            <div class="btn-group">
                <button type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ra ra-cycle" data-toggle="tooltip" data-placement="top" title="Remove from Warband"></i>
                    <span class="caret"></span>
                    <span class="sr-only">Dismiss</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="" data-id="{$Warband.Apprentice.ID}" class="ApprenticeDismiss" data-toggle="tooltip" data-placement="top" title="Dismiss the Apprentice">
                            <i class="ra ra-cycle"></i> Dismiss the Apprentice
                        </a>
                    </li>
                </ul>
            </div>
            <% end_if %>

        </div>
    </div>
</div>



<div class="unit-block">
    <div class="unit-block-content">

        <table class="table unit-table">
            <thead class="{$cssClass($Warband.School.Name)} bgCol">
                <tr>
                    <th class="name">The
                        <% if $Unit.Type == 'Captain' %>Level $Top.Warband.getCLevel <% end_if %>
                        <% if $Unit.Type == 'Wizard' %>Level $Top.Level <% end_if %>
                        $Unit.Type
                    </th>
                    <th class="move text-center" data-toggle="tooltip" data-placement="top" title="Move">M<span class="visible-print">ove</span></th>
                    <th class="fight text-center" data-toggle="tooltip" data-placement="top" title="Fight">F<span class="visible-print">ight</span></th>
                    <th class="shoot text-center" data-toggle="tooltip" data-placement="top" title="Shoot">S<span class="visible-print">hoot</span></th>
                    <th class="armour text-center" data-toggle="tooltip" data-placement="top" title="Armour">A<span class="visible-print">rmour</span></th>
                    <th class="will text-center" data-toggle="tooltip" data-placement="top" title="Will">W<span class="visible-print">ill</span></th>
                    <th class="health text-center" data-toggle="tooltip" data-placement="top" title="Health">H<span class="visible-print">ealth</span></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><% include RenameUnit ID=$Unit.ID,Name=$Unit.Name %><em class="unitsCurrentName">{$Unit.Name}</em></td>
                    <td class="text-center stat">{$Unit.Move}<% if $Unit.Move != $Unit.MoveAdjusted %>/$Unit.MoveAdjusted<% end_if %></td>
                    <td class="text-center stat">{$Unit.Fight}<% if $Unit.Fight != $Unit.FightAdjusted %>/$Unit.FightAdjusted<% end_if %></td>
                    <td class="text-center stat">{$Unit.Shoot}<% if $Unit.Shoot != $Unit.ShootAdjusted %>/$Unit.ShootAdjusted<% end_if %></td>
                    <td class="text-center stat">{$Unit.Armour}<% if $Unit.Armour != $Unit.ArmourAdjusted %>/$Unit.ArmourAdjusted<% end_if %></td>
                    <td class="text-center stat">{$Unit.Will}<% if $Unit.Will != $Unit.WillAdjusted %>/$Unit.WillAdjusted<% end_if %></td>
                    <td class="text-center stat">{$Unit.Health}<% if $Unit.Health != $Unit.HealthAdjusted %>/$Unit.HealthAdjusted<% end_if %></td>
                </tr>



                <% if $Unit.Type == 'Captain' %>


                <% if $Warband.Captain.captainTricks %>
                <tr>
                    <td colspan="7">
                        <h5>Tricks</h5>
                        <table class="table unit-table">
                            <thead>
                                <tr>

                                    <th>Trick</th>
                                    <th>Effect</th>
                                    <th>When to Declare</th>
                                    <th class="hidden-print"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% loop $Warband.Captain.captainTricks %>
                                <tr <% if $Used == 1 %>class="soldierInjured"<% end_if %>>
                                    <td><strong>{$Trick.Name}</strong></td>
                                    <td>{$Trick.Desc}</td>
                                    <td>{$Trick.Declare}</td>
                                    <td class="hidden-print text-right">
                                        <a href="" data-id="{$ID}" class="useTrick btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Use or reset"><i class="ra ra-focused-lightning"></i> <% if $Used == 1 %>Reset<% else %>Use<% end_if %></a><a href="" data-id="{$ID}" class="removeTrick btn btn-xs btn-danger hidden-print" data-toggle="tooltip" data-placement="top" title="Remove this trick"><i class="ra ra-cycle"></i></a>
                                    </td>
                                </tr>
                                <% end_loop %>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <% end_if %>

                <% if $Unit.MarkID > 0 %>
                <tr>
                    <td colspan="7">
                        <h5>Mark</h5>
                        <table class="table unit-table">
                            <thead>
                            <tbody>
                                <tr class="MarkList" data-unit="{$Unit.ID}">
                                    <td><strong>$Unit.Mark.Type $Unit.Mark.Name</strong></td>
                                    <td>{$Unit.Mark.Desc}</td>
                                    <td class="text-right"><a href="" data-mark="0" class="removeMark btn btn-xs btn-danger hidden-print" data-toggle="tooltip" data-placement="top" title="Remove this Mark"><i class="ra ra-cycle"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <% end_if %>

                <% end_if %>
                <tr>
                    <td colspan="7">
                        <div class="row">
                            <div class="col-sm-12 inlinedropdowns" data-id="{$Unit.ID}">
                                <div class="h5">Items <% if $Unit.isDuelWeapon %><i class="ra ra-crossed-swords text-danger" data-toggle="tooltip" data-placement="top" title="Hand weapon + dagger"></i><% end_if %></div>
                                <% if $Unit.numItemsAssigned >= 1 %>
                                <% loop $Unit.itemsAssigned %>
                                <div class="dropdown item">
                                    <button href="#" class="dropdown-toggle btn btn-sm <% if $Equipped == 1%>btn-success<% else %>btn-default<% end_if %>" id="Item-{$ID}-{$Item.ID}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="ra {$Item.Icon}"></i> {$Item.Name}<% if $Spell %><span> of {$Spell.Name}</span><% end_if %> <span class="caret"></span></button>
                                    <ul class="dropdown-menu" id="Item-drop-{$ID}-{$Item.ID}" aria-labelledby="Item-{$ID}-{$Item.ID}">
                                        <li>
                                            <div>
                                                <h5><i class="ra {$Item.Icon}"></i> {$Item.Name}
                                                    <% if $Spell %><span> of {$Spell.Name}</span><% end_if %>
                                                </h5>
                                            </div>
                                            <div>{$Item.Desc}</div>
                                            <div class="btn-group">
                                                <% include itemoptions %>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <% end_loop %>
                                <% end_if %>

                                <% if $Top.Warband.hasMount %>
                                <% if $Unit.MountID > 0 %>
                                <a href="" class="removeMount btn btn btn-warning hidden-print" data-id="{$Unit.ID}"><i class="ra ra-horseshoe"></i> Dismount</a>
                                <% else %>
                                <% loop $Top.Warband.getMounts %>
                                <a href="" class="addMount btn btn btn-warning hidden-print" data-id="{$item.ID}"><i class="ra ra-horseshoe"></i> Mount on $item.Name</a>
                                <% end_loop %>
                                <% end_if %>
                                <% end_if %>

                                <% if $Unit.Type == 'Captain' %>
                                <% if $Top.Warband.canBrand == true %>

                                <div class="btn-group">
                                    <button type="button" class="dropdown-toggle addMarkActivator btn btn-sm btn-success hidden-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <% if $Unit.MarkID > 0 %><i class="ra ra-<% if $Unit.Mark.Type == 'Burning' %>fire<% else %>bleeding-hearts<% end_if %>"></i> $Unit.Mark.Name<% else %><i class="ra ra-level-four"></i> Add Mark<% end_if %>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-left MarkList" data-unit="{$Unit.ID}">
                                        <% loop $MarksList %>
                                        <li><a href="" data-mark="{$ID}" class="addMark" title="{$Desc}"><i class="ra ra-<% if $Type == 'Burning' %>fire<% else %>bleeding-hearts<% end_if %>"></i> {$Name} ({$Type})</a></li>
                                        <% end_loop %>
                                        <% if $Unit.MarkID > 0 %>
                                        <li><a href="" data-mark="0" class="removeMark" title="Remove this Mark"><i class="ra ra-level-four"></i> Remove Mark</a></li>
                                        <% end_if %>
                                    </ul>
                                </div>
                                <% end_if %>
                                <% end_if %>
                            </div>
                        </div>
                        <% if $Unit.Injuries.Count > 0 %>
                        <div class="row">
                            <div class="col-sm-12 inlinedropdowns">
                                <div class="h5">Injuries</div>
                                
                                <% loop $Unit.Injuries %>
                                <div class="dropdown item">
                                    <button href="#" class="dropdown-toggle btn btn-sm <% if $Treated == 1 %>treated<% end_if %>" id="Injury-{$ID}-{$Injury.ID}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="ra ra-cut-palm <% if $Treated == 1 %>text-success<% else %>text-danger<% end_if %>"></i> {$Injury.Name} <span class="caret"></span></button>
                                    <ul class="dropdown-menu" id="Injury-drop-{$ID}-{$Injury.ID}" aria-labelledby="Injury-{$ID}-{$Item.ID}">
                                        <li>
                                            <div>
                                                <h5><i class="ra ra-cut-palm text-danger"></i> {$Injury.Name} x $Num</h5>
                                            </div>
                                            <div>{$Injury.Desc}</div>
                                            <div class="btn-group">
                                                <% include injuryoptions %>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <% end_loop %>
                                <% end_if %>
                                <% if $Unit.Type == 'Wizard' %>
                                <% if $Warband.BeastcrafterLevelID > 0 %>
                                <h5>$Warband.BeastcrafterLevel.Name</h5>
                                <div class="hidden">$Warband.BeastcrafterLevel.Desc</div>
                                <% if $Warband.beastcrafterTraits %>
                                <div class="dropdown item">
                                    <button href="#" class="dropdown-toggle btn btn-sm" id="beastcraftertraitselectbutton" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="ra ra-pawprint"></i> <% if $Warband.BeastcrafterTraitID > 0 %>$Warband.BeastcrafterTrait.Name<% else %>Beastcrafter trait<% end_if %> <span class="caret"></span></button>
                                    <ul class="dropdown-menu" id="beastcraftertraitselect" aria-labelledby="beastcraftertraitselect">
                                        <li><a href="" data-id="0" class="addbeastcraftertrait"><i class="ra ra-pawprint text-warning"></i> none</a></li>
                                        <% loop $Warband.beastcrafterTraits %>
                                        <li><a href="" data-id="{$ID}" class="addbeastcraftertrait"><i class="ra ra-pawprint text-warning"></i> $Name</a></li>
                                        <% end_loop %>
                                    </ul>
                                </div>
                                <% if $Warband.BeastcrafterTraitID != 0 %>
                                <div class="small"><strong>$Warband.BeastcrafterTrait.Name</strong> $Warband.BeastcrafterTrait.Desc</div>
                                <% end_if %>
                                
                                <% end_if %>
                                <% end_if %>

                            </div>
                        </div>
                        <% end_if %>
                        <% if $Warband.DemonicPact == 1 && $Unit.Type == 'Wizard' %>
                        <% if $Warband.Boons %>
                        <div class="h5">Boons</div>
                        <table class="table unit-table">
                            <thead>
                                <tr>

                                    <th>Boon</th>
                                    <th>Effect</th>
                                    <th class="hidden-print"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% loop $Warband.Boons %>
                                <tr>
                                    <td><strong>{$Boon.Name}</strong></td>
                                    <td>{$Boon.Desc}</td>
                                    <td class="hidden-print text-right">
                                        <a href="" data-id="{$ID}" class="removeBoon btn btn-xs btn-danger hidden-print" data-toggle="tooltip" data-placement="top" title="Remove this boon"><i class="ra ra-cycle"></i></a>
                                    </td>
                                </tr>
                                <% end_loop %>
                            </tbody>
                        </table>


                        <% end_if %>
                        <% if $Warband.Sacrifices %>
                        <div class="h5">Sacrifices</div>
                        <table class="table unit-table">
                            <thead>
                                <tr>

                                    <th>Boon</th>
                                    <th>Effect</th>
                                    <th class="hidden-print"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% loop $Warband.Sacrifices %>
                                <tr>
                                    <td><strong>{$Sacrifice.Name}</strong></td>
                                    <td>{$Sacrifice.Desc}</td>
                                    <td class="hidden-print text-right">
                                        <a href="" data-id="{$ID}" class="removeBoon btn btn-xs btn-danger hidden-print" data-toggle="tooltip" data-placement="top" title="Remove this boon"><i class="ra ra-cycle"></i></a>
                                    </td>
                                </tr>
                                <% end_loop %>
                            </tbody>
                        </table>
                        <% end_if %>




                        <% end_if %>




                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>


