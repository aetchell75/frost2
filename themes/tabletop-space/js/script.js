$(document).ready(function () {
    if($(".owl-carousel").length > 0) {
        $(".owl-carousel").owlCarousel({
            items: 1,
            loop:true,
            dots:true,
            autoplay:true,
            autoplayTimeout: 8000
        });        
    }
	$(document).on('click', '.yamm .dropdown-menu', function(e) {
	  e.stopPropagation()
	});
        $('.matchHeight').matchHeight();
        
        $('.eq-height').each(function() {
            $(this).children('.matchHeight').matchHeight({
                byRow: true
            });
        });        

});